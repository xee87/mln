---- 12/17/2019

ALTER TABLE `user_settings` ADD `timezone` VARCHAR(500) NULL AFTER `time`;

---- 12/18/2019

ALTER TABLE `purchase` CHANGE `timestamp` `timestamp` VARCHAR(500) NULL DEFAULT NULL;
ALTER TABLE `purchase` CHANGE `message` `message` VARCHAR(500) NULL DEFAULT NULL;

ALTER TABLE `user_settings` CHANGE `eventId` `targetId` VARCHAR(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `users` ADD `status` INT NOT NULL DEFAULT '1' COMMENT '1=> active, 0 => Inactive' AFTER `remember_token`;


------ 19 / 03 / 2020
ALTER TABLE `users` CHANGE `name` `name` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;

--     05/08/2020

ALTER TABLE `users` ADD `phone_verified_at` TIMESTAMP NULL AFTER `email_verified_at`;
ALTER TABLE `users` ADD `verification_code` VARCHAR(500) NULL AFTER `remember_token`;
