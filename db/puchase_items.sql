-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 17, 2019 at 05:06 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";




/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mln`
--

-- --------------------------------------------------------

--
-- Table structure for table `puchase_items`
--

DROP TABLE IF EXISTS `puchase_items`;
CREATE TABLE IF NOT EXISTS `puchase_items` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `purchase_id` int(100) NOT NULL,
  `sr_no` varchar(500) DEFAULT NULL,
  `pde` varchar(500) DEFAULT NULL,
  `name` varchar(500) DEFAULT NULL,
  `list_price` varchar(500) DEFAULT NULL,
  `discount` varchar(500) DEFAULT NULL,
  `your_price` varchar(500) DEFAULT NULL,
  `soh` varchar(500) DEFAULT NULL,
  `quantity` varchar(500) DEFAULT NULL,
  `subtotal_ex` varchar(500) DEFAULT NULL,
  `subtotal_inc` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
