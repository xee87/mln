-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 05, 2020 at 12:03 AM
-- Server version: 5.7.26
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pharmacy_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pharmacy_name` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pharmacy_phone` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pharmacy_email` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pbs_number` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_state` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1=>nonWholesalers, 2 => agreed on terms',
  `trail_expires` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 => ontrail, 1 => reminder sent, 2 => trail over sent',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1=> active, 0 => Inactive, 2 => delete,',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `pharmacy_name`, `pharmacy_phone`, `pharmacy_email`, `pbs_number`, `address`, `role`, `remember_token`, `user_state`, `trail_expires`, `status`, `created_at`, `updated_at`) VALUES
(9, 'User', 'broowsing@live.com', NULL, '$2y$10$wC32rD0j3IxjMRHzH/Ww5.ChJvnrhoRFnggFACwZjVlj6V7qmeZ3i', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2019-12-16 10:02:01', '2020-01-07 05:38:46'),
(8, 'admin', 'admin@mln.com', NULL, '$2y$10$3Hl9Ob5QRnSJX4gbc2tDE.Ws/pWUVcVQemD.IvK3gXYXfFmYxSPcK', NULL, NULL, NULL, NULL, NULL, NULL, 'InMnbiqzj91XL5h8j3OwOYolgmkV2FIWr1x8im6l145DH5IpSl0enBS9Pzu0', 0, 0, 1, '2019-12-13 13:29:54', '2019-12-13 13:29:54'),
(21, 'Amer R', 'amer@mail.com', NULL, '$2y$10$SS1BHhmGQk6.HHE/uYrVD.wBwSwqS8n5Qb00peDtDN2VCrq39bHmy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2020-01-07 05:43:34', '2020-01-10 18:48:05'),
(10, 'JNT', 'jntqa@outlook.com', NULL, '$2y$10$mvwO3CcEJBahnFr100Rdpef4d0AJ9tGkvwmhc1V7bahLNvwa00592', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2019-12-18 19:55:34', '2020-01-10 18:49:56'),
(11, 'danish-ali', 'danish-ali@hotmail.com', NULL, '$2y$10$igEtqRnXwp1Q8BEGIziWfen79aDqiyLPBToC.JhPV4ikxjGr3LMG2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2019-12-20 17:18:55', '2020-01-10 18:48:27'),
(12, 'Nicole C.', 'Nicolec@gmail.com', NULL, '$2y$10$uBPvZCwKNJdAGGZJaX34q.IyvYHFNp/zEOYGNINkkxUmk6bVo8D/W', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2019-12-20 17:23:52', '2020-01-10 18:48:37'),
(13, 'Jamie R.', 'Jamier@live.com', NULL, '$2y$10$bWL82RxvW8jgzSgPPbgDiu1Dp3eToWjMwZ0ROam1QsqMjc.LJ1Qfm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2019-12-20 17:24:47', '2020-01-10 18:47:44'),
(14, 'Amer R', 'johnnuman@live.com', NULL, '$2y$10$kRnndonGesT5kzbiv3wcnehXgfxHXRm9khiApn9GjLluDNsyutHX.', NULL, NULL, NULL, NULL, NULL, NULL, 'yuL05bZTkqBkaRjz8vT84oVrRXqokyJbMuXBw8Nawxe2UR4LNW1yeB3e1aV6', 0, 0, 1, '2019-12-20 17:29:47', '2020-02-13 21:39:04'),
(15, 'Natalie E.', 'NatalieE@hotmail.com', NULL, '$2y$10$MXr1aSym23LfHLHeErbJAOOPeifMDmG9Mg3sI.zIwSAfPm3rYY1x6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2019-12-20 17:30:50', '2020-01-10 18:48:56'),
(16, 'Noel B.', 'Noelb@gmail.com', NULL, '$2y$10$rjC6p.Vk1d.CyBhWfMryH.nBsLC9biQ4D0JNRM3sFoxkKtk/2MtoK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2019-12-20 18:19:21', '2020-01-10 18:49:36'),
(17, 'jennifer h.', 'jenniferh@live.com', NULL, '$2y$10$kuGWjVZYTbF8wb4MBBBB1O/cbwSXF46zdpibSPsYa5iaDkZGv6Dmy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2019-12-20 18:20:04', '2020-01-10 18:49:16'),
(33, 'Danish', 'newlive@gmail.com', '2020-03-03 06:15:33', '$2y$10$cmaPHvAB.TL2YnqHY4lekuC9EGwGh12DSp62aVLrQKmmivmFj45Ca', 'Social Pharmacy', '+92262000', 'yes@gmail.com', '255569', 'Muslim Town Kotly Behram', 'Pharmist', 'j4VJIHTHxPLbdQmr7i2pC9RSsOHDPIHTSdzAcPazWXzZqULnfJCsZ929r1Yy', 0, 0, 1, '2020-03-03 06:08:44', '2020-03-03 08:23:02'),
(32, 'Danish Rehman', 'yesdon2@gmail.com', '2020-03-03 06:16:30', '$2y$10$jxFRd800/9Qroc2y30Xu6eAHDsdqvrYqJ77NPNZqsMsLamt42Hima', 'Social Pharmacy', '9993625', 'sa@gmail.com', NULL, 'San Francisco, CA, USA', 'Pharmist', NULL, 0, 0, 1, '2020-03-03 05:54:59', '2020-03-04 04:07:19'),
(31, NULL, 'hello1@gmail.com', NULL, '$2y$10$BpDrdR4uOQGgM/tjynD.XeIOKl2DhrE6cox8K90jXRLhTYlVf2lGG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2020-03-03 05:45:50', '2020-03-03 05:45:50'),
(30, NULL, 'ahmad@gmail.com', NULL, '$2y$10$shgWYsj4wgI8s5OZcgWR.ensEHj759mv69Fee8g/D6ir/x9FLDo1S', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2020-03-03 05:35:26', '2020-03-03 05:35:26'),
(29, NULL, 'danish@gmail.com', '2020-03-01 08:23:53', '$2y$10$5.iPYz36keNTcm.taiE9/eGoAAl7aEdKJ6j/F/4BbgYWhFDGNdbBe', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 1, '2020-03-03 05:34:01', '2020-03-04 06:23:21'),
(34, 'Ahmad Bilal', 'testfinal@gmail.com', '2020-03-01 08:23:53', '$2y$10$exX8wCgC9t/bkiCqdRYBqu9WGwWEwQhdy9HXeO7o3QS91nj/KDeXK', 'Bilal Store', '+92-366251', 'b-store@gmail.com', '0002444', 'Muslim Town Kotly Behram', 'Pharmist', '5whmeTfNfWVwDSVOPC9DuKT5s4sBwZCrGA5vLcwvp58nmKROHrx7hCBk49Lg', 2, 1, 1, '2020-03-03 08:23:34', '2020-03-04 06:23:17'),
(35, 'Shehzad Ahmad1', 'shehzad@gmail.com', '2020-02-20 01:18:43', '$2y$10$pBny1HvzvsKAoE5PpdF3.ewHbGrcStVfIhXNpfNb6sAXQVgd9RjBa', 'Social Pharmacy1', '99936251', 'mrdanish@gmai1l.com', NULL, 'Sialkot1', 'Pharmist1', 'HmlRfIsmVxXQYYRmx2rIwaRsrIbIkROqJDUc4udhzUT0tNbfJpd59kSBfEz9', 2, 2, 1, '2020-03-04 01:18:06', '2020-03-04 07:04:18'),
(36, NULL, 'tesst@gmail.com', '2020-03-04 03:53:34', '$2y$10$1TCecgV.Lv/L2n3PdCgbgeLH0qOQ1e1Bhm7AnMBKwgmCAxXEe8Oh2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2020-03-04 03:53:19', '2020-03-04 03:53:49'),
(37, 'Zeeshan Shahid', 'zeeshan@gmail.com', '2020-03-04 07:55:47', '$2y$10$2F//FEWnDwyfMizXJhsF8e7J5jKlk9w5/pDJlLpAh/fbM5mIlKXXq', 'Social Pharmacy', '9266666', 'zee@gmail.com', '99996', 'Sialkot Motorway Bypass, Lahore, Pakistan', 'Pharmist', 'zIOPcaiNyPg41CPf1wY9j6S6Kqy53EEHsUECmSjqPs1Zfb1LbllaB7IQ4qo2', 2, 0, 1, '2020-03-04 07:55:28', '2020-03-04 08:03:14');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
