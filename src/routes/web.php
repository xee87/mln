<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::get('/', 'HomeController@index')->name('home');
Route::get('/about-us', 'HomeController@about_us')->name('about.us');
Route::get('/contact-us', 'HomeController@contact_us')->name('contact.us');
Route::post('/contact-us/save', 'HomeController@contactSave')->name('contact.save');
Route::get('/faq', 'HomeController@faq')->name('faq');
Route::get('/user-register', 'HomeController@register')->name('user.register');
Route::get('/user-login','HomeController@login')->name('user.login');


Route::get('/aws/event-test' ,'AWSCloudWatchController@eventTest')->name('aws.event');


Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@dashboard')->name('dashboard');
Route::get('/debug', 'HomeController@debug')->name('debug');

Route::get('/email-testing', 'HomeController@emailTesting')->name('email.test');
Route::post('/email-get', 'HomeController@emailGet')->name('email.get');

Route::get('/register/{id}/info', 'UserController@info')->name('register.info');
Route::post('/register-info/save', 'UserController@store_info')->name('register.info.store');
Route::any('/non-wholesalers/{id}/message', 'UserController@messageView')->name('register.message');
Route::any('/acceptTerms', 'HomeController@ajaxTermsCondition')->name('acceptTerms');
Route::any('/mail-jobs', 'JobController@processQueue')->name('job.expire-trails');
Route::post('/email-user' , 'HomeController@emailUser')->name('email.user');


Route::any('/send-verify-code', 'PhoneVerificationController@ajaxSendVerifyCode')->name('verifyphone.send');
Route::any('/verify-code', 'PhoneVerificationController@verify')->name('verifyphone.verify');




//Route::get('/save-purchase', 'PurchaseController@save_purchase')->name('save.purchase');



Route::prefix('/dashboard')->middleware('role:Admin','auth')->group(function () {

	//User Management Routing.
	Route::resource('user', 'UserController', ['except' => ['show']]);

	Route::any('user/export','UserController@export')->name('user.export');
	Route::any('users/retailers','UserController@nonwholesalers')->name('user.retailers');

	Route::any('user/delete/{id}','UserController@delete')->name('user.delete');
	Route::any('user/active/{id}','UserController@active')->name('user.active');
	Route::any('user/inactive/{id}','UserController@inactive')->name('user.inactive');
	Route::any('user/edit/{id}','UserController@editUser')->name('user.edit');
	Route::post('user/search','UserController@search')->name('user.search');




	Route::get('settings', 'SettingController@index')->name('settings');
	Route::get('email-settings', 'SettingController@emailSettings')->name('settings.email');
	Route::post('update-options', 'SettingController@update_options')->name('settings.update');


});

Route::prefix('/dashboard')->middleware('auth')->group(function () {

	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);

});

Route::prefix('/dashboard')->middleware('role:User','auth')->group(function () {

	Route::any('api-portal-settings', 'UserSettingController@index')->name('user.settings');
	Route::post('settings/create', 'UserSettingController@create')->name('user.settings.create');
	Route::post('settings/update', 'UserSettingController@update')->name('user.settings.update');

    Route::any('/saas-agreement','UserController@agreementPdf')->name('pdf.agreement');

	Route::get('purchase', 'PurchaseController@index')->name('purchase');
	Route::get('purchase/{id}', 'PurchaseController@purchase_items')->name('purchase.items');


});
