<div class="modal fade" id="termConditionModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
    <div class="modal-dialog modal-lg modal- modal-dialog-centered modal-" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h3 class="modal-title" id="modal-title-default">Terms and Conditions</h3>
                {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                    {{--<span aria-hidden="true">×</span>--}}
                {{--</button>--}}
            </div>

            <div class="modal-body">

               <div class="terms-condition-content">
                   <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                               name=about-the-website><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>1.<span
                                           style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>About
                           the Website</a></h2>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo23'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>1.1.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Welcome to www.pharmaanywhere.com.au (the '<b
                               style='mso-bidi-font-weight:normal'>Website</b>'). The Website Retail
                       Pharmacies Management Web Applications (the '<b style='mso-bidi-font-weight:
normal'>Services</b>').</p>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo23'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>1.2.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>The Website is operated by<span
                               style='mso-spacerun:yes'>  </span>Pharma Anywhere (ABN 67358896297) . Access to
                       and use of the Website, or any of its associated Products or Services, is
                       provided by Pharma Anywhere. Please read these terms and conditions (the '<b
                               style='mso-bidi-font-weight:normal'>Terms</b>') carefully. By using, browsing
                       and/or reading the Website, this signifies that you have read, understood and
                       agree to be bound by the Terms. If you do not agree with the Terms, you must
                       cease usage of the Website, or any of Services, immediately.</p>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo23'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>1.3.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Pharma Anywhere reserves the right to review and
                       change any of the Terms by updating this page at its sole discretion. When
                       Pharma Anywhere updates the Terms, it will use reasonable endeavours to provide
                       you with notice of updates to the Terms. Any changes to the Terms take
                       immediate effect from the date of their publication. Before you continue, we recommend
                       you keep a copy of the Terms for your records.</p>

                   <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                               name=acceptance-of-the-terms><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>2.<span
                                           style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Acceptance
                           of the Terms</a></h2>

                   <p class=MsoNormal style='margin-left:24.0pt;text-indent:-24.0pt;mso-list:l2 level1 lfo21;
tab-stops:list 0in'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'><span
                                       style='mso-spacerun:yes'> </span><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>You accept the Terms by remaining on the
                       Website. You may also accept the Terms by clicking to accept or agree to the
                       Terms where this option is made available to you by Pharma Anywhere in the user
                       interface.</p>

                   <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                               name=subscription-to-use-the-services><![if !supportLists]><span
                                   style='mso-fareast-font-family:Arial;mso-bidi-font-family:Arial'><span
                                       style='mso-list:Ignore'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Subscription to use the Services</a></h2>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo24'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>3.1.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>In order to access the Services, you must first
                       purchase a subscription through the Website (the '<b style='mso-bidi-font-weight:
normal'>Subscription</b>') and pay the applicable fee for the selected
                       Subscription (the '<b style='mso-bidi-font-weight:normal'>Subscription Fee</b>').</p>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo24'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>3.2.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>In purchasing the Subscription, you acknowledge
                       and agree that it is your responsibility to ensure that the Subscription you
                       elect to purchase is suitable for your use.</p>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo24'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>3.3.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Once you have purchased the Subscription, you
                       will then be required to register for an account through the Website before you
                       can access the Services (the ' <b style='mso-bidi-font-weight:normal'>Account</b>').</p>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo24'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>3.4.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>As part of the registration process, or as part
                       of your continued use of the Services, you may be required to provide personal
                       information about yourself (such as identification or contact details),
                       including:</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo25'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(a)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Email
                       address</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo25'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(b)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Preferred
                       username</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo25'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(c)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Mailing
                       address</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo25'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(d)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Telephone
                       number</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo25'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(e)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Password</p>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo24'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>3.5.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>You warrant that any information you give to
                       Pharma Anywhere in the course of completing the registration process will
                       always be accurate, correct and up to date.</p>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo24'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>3.6.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Once you have completed the registration
                       process, you will be a registered member of the Website ('<b style='mso-bidi-font-weight:
normal'>Member</b>') and agree to be bound by the Terms. As a Member you will
                       be granted immediate access to the Services from the time you have completed
                       the registration process until the subscription period expires (the '<b
                               style='mso-bidi-font-weight:normal'>Subscription Period</b>').</p>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo24'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>3.7.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>You may not use the Services and may not accept
                       the Terms if:</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo26'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(a)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>you
                       are not of legal age to form a binding contract with Pharma Anywhere; or</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo26'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(b)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>you
                       are a person barred from receiving the Services under the laws of Australia or
                       other countries including the country in which you are resident or from which
                       you use the Services.</p>

                   <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                               name=your-obligations-as-a-member><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>4.<span
                                           style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Your
                           obligations as a Member</a></h2>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo27'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>4.1.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>As a Member, you agree to comply with the
                       following:</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo28'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(a)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>you
                       will use the Services only for purposes that are permitted by:</p>

                   <p class=MsoNormal style='margin-left:132.0pt;text-indent:-24.0pt;mso-list:
l3 level4 lfo29;tab-stops:list 1.5in'><![if !supportLists]><span
                               style='mso-fareast-font-family:Arial;mso-bidi-font-family:Arial'><span
                                   style='mso-list:Ignore'>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>the Terms; and</p>

                   <p class=MsoNormal style='margin-left:132.0pt;text-indent:-24.0pt;mso-list:
l3 level4 lfo29;tab-stops:list 1.5in'><![if !supportLists]><span
                               style='mso-fareast-font-family:Arial;mso-bidi-font-family:Arial'><span
                                   style='mso-list:Ignore'>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>any applicable law, regulation or generally
                       accepted practices or guidelines in the relevant jurisdictions;</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo28'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(b)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>you
                       have the sole responsibility for protecting the confidentiality of your
                       password and/or email address. Use of your password by any other person may
                       result in the immediate cancellation of the Services;</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo28'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(c)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>any
                       use of your registration information by any other person, or third parties, is
                       strictly prohibited. You agree to immediately notify Pharma Anywhere of any
                       unauthorised use of your password or email address or any breach of security of
                       which you have become aware;</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo28'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(d)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>access
                       and use of the Website is limited, non-transferable and allows for the sole use
                       of the Website by you for the purposes of Pharma Anywhere providing the
                       Services;</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo28'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(e)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>you
                       will not use the Services or the Website in connection with any commercial
                       endeavours except those that are specifically endorsed or approved by the
                       management of Pharma Anywhere;</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo28'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(f)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>you
                       will not use the Services or Website for any illegal and/or unauthorised use
                       which includes collecting email addresses of Members by electronic or other
                       means for the purpose of sending unsolicited email or unauthorised framing of
                       or linking to the Website;</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo28'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(g)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>you
                       agree that commercial advertisements, affiliate links, and other forms of
                       solicitation may be removed from the Website without notice and may result in
                       termination of the Services. Appropriate legal action will be taken by Pharma
                       Anywhere for any illegal or unauthorised use of the Website; and</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo28'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(h)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>you
                       acknowledge and agree that any automated use of the Website or its Services is
                       prohibited.</p>

                   <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                               name=payment><![if !supportLists]><span style='mso-fareast-font-family:Arial;
mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>5.<span
                                           style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Payment</a></h2>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo30'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>5.1.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Where the option is given to you, you may make
                       payment of the Subscription Fee by way of:</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo31'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(a)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Credit
                       Card Payment ('<b style='mso-bidi-font-weight:normal'>Credit Card</b>')</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo31'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(b)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>PayPal
                       ('<b style='mso-bidi-font-weight:normal'>PayPal</b>')</p>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo30'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>5.2.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>All payments made in the course of your use of
                       the Services are made using eWAY. In using the Website, the Services or when
                       making any payment in relation to your use of the Services, you warrant that
                       you have read, understood and agree to be bound by the eWAY terms and
                       conditions which are available on their website.</p>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo30'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>5.3.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>You acknowledge and agree that where a request
                       for the payment of the Subscription Fee is returned or denied, for whatever
                       reason, by your financial institution or is unpaid by you for any other reason,
                       then you are liable for any costs, including banking fees and charges,
                       associated with the Subscription Fee .</p>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo30'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>5.4.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>You agree and acknowledge that Pharma Anywhere
                       can vary the Subscription Fee at any time<span style='mso-spacerun:yes'> 
</span>and that the varied Subscription Fee will come into effect following the
                       conclusion of the existing Subscription Period.</p>

                   <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                               name=refund-policy><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>6.<span
                                           style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Refund
                           Policy</a></h2>

                   <p class=MsoNormal style='margin-left:24.0pt;text-indent:-24.0pt;mso-list:l2 level1 lfo21;
tab-stops:list 0in'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'><span
                                       style='mso-spacerun:yes'> </span><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Pharma Anywhere will only provide you with a
                       refund of the Subscription Fee in the event they are unable to continue to provide
                       the Services or if the manager of Pharma Anywhere makes a decision, at its
                       absolute discretion, that it is reasonable to do so under the circumstances .
                       Where this occurs, the refund will be in the proportional amount of the
                       Subscription Fee that remains unused by the Member (the '<b style='mso-bidi-font-weight:
normal'>Refund</b>').</p>

                   <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                               name=copyright-and-intellectual-property><![if !supportLists]><span
                                   style='mso-fareast-font-family:Arial;mso-bidi-font-family:Arial'><span
                                       style='mso-list:Ignore'>7.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Copyright and Intellectual Property</a></h2>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo32'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>7.1.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>The Website, the Services and all of the related
                       products of Pharma Anywhere are subject to copyright. The material on the
                       Website is protected by copyright under the laws of Australia and through
                       international treaties. Unless otherwise indicated, all rights (including
                       copyright) in the Services and compilation of the Website (including but not
                       limited to text, graphics, logos, button icons, video images, audio clips,
                       Website, code, scripts, design elements and interactive features) or the
                       Services are owned or controlled for these purposes, and are reserved by Pharma
                       Anywhere or its contributors.</p>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo32'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>7.2.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>All trademarks, service marks and trade names
                       are owned, registered and/or licensed by Pharma Anywhere, who grants to you a
                       worldwide, non-exclusive, royalty-free, revocable license whilst you are a
                       Member to:</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo33'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(a)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>use
                       the Website pursuant to the Terms;</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo33'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(b)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>copy
                       and store the Website and the material contained in the Website in your
                       device's cache memory; and</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo33'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(c)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>print
                       pages from the Website for your own personal and non-commercial use.</p>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo32'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>7.3.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Pharma Anywhere does not grant you any other
                       rights whatsoever in relation to the Website or the Services. All other rights
                       are expressly reserved by Pharma Anywhere.</p>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo32'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>7.4.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Pharma Anywhere retains all rights, title and
                       interest in and to the Website and all related Services. Nothing you do on or
                       in relation to the Website will transfer any:</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo34'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(a)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>business
                       name, trading name, domain name, trade mark, industrial design, patent, registered
                       design or copyright, or</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo34'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(b)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>a
                       right to use or exploit a business name, trading name, domain name, trade mark
                       or industrial design, or</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo34'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(c)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>a
                       thing, system or process that is the subject of a patent, registered design or
                       copyright (or an adaptation or modification of such a thing, system or
                       process),</p>

                   <p class=MsoNormal style='margin-left:60.0pt;text-indent:-24.0pt;mso-list:l2 level2 lfo21;
tab-stops:list .5in'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'><span
                                       style='mso-spacerun:yes'> </span><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>to you.</p>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo32'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>7.5.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>You may not, without the prior written
                       permission of Pharma Anywhere and the permission of any other relevant rights
                       owners: broadcast, republish, up-load to a third party, transmit, post,
                       distribute, show or play in public, adapt or change in any way the Services or
                       third party Services for any purpose, unless otherwise provided by these Terms.
                       This prohibition does not extend to materials on the Website, which are freely
                       available for re-use or are in the public domain.</p>

                   <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                               name=privacy><![if !supportLists]><span style='mso-fareast-font-family:Arial;
mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>8.<span
                                           style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Privacy</a></h2>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo35'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>8.1.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Pharma Anywhere takes your privacy seriously and
                       any information provided through your use of the Website and/or Services are
                       subject to Pharma Anywhere's Privacy Policy, which is available on the Website.</p>

                   <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                               name=general-disclaimer><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>9.<span
                                           style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>General
                           Disclaimer</a></h2>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo36'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>9.1.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Nothing in the Terms limits or excludes any
                       guarantees, warranties, representations or conditions implied or imposed by
                       law, including the Australian Consumer Law (or any liability under them) which
                       by law may not be limited or excluded.</p>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo36'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>9.2.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Subject to this clause, and to the extent
                       permitted by law:</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo37'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(a)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>all
                       terms, guarantees, warranties, representations or conditions which are not
                       expressly stated in the Terms are excluded; and</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo37'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(b)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Pharma
                       Anywhere will not be liable for any special, indirect or consequential loss or
                       damage (unless such loss or damage is reasonably foreseeable resulting from our
                       failure to meet an applicable Consumer Guarantee), loss of profit or
                       opportunity, or damage to goodwill arising out of or in connection with the
                       Services or these Terms (including as a result of not being able to use the
                       Services or the late supply of the Services), whether at common law, under
                       contract, tort (including negligence), in equity, pursuant to statute or
                       otherwise.</p>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo36'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>9.3.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Use of the Website and the Services is at your
                       own risk. Everything on the Website and the Services is provided to you
                       &quot;as is&quot; and &quot;as available&quot; without warranty or condition of
                       any kind. None of the affiliates, directors, officers, employees, agents,
                       contributors and licensors of Pharma Anywhere make any express or implied
                       representation or warranty about the Services or any products or Services
                       (including the products or Services of Pharma Anywhere) referred to on the
                       Website. includes (but is not restricted to) loss or damage you might suffer as
                       a result of any of the following:</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo38'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(a)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>failure
                       of performance, error, omission, interruption, deletion, defect, failure to
                       correct defects, delay in operation or transmission, computer virus or other
                       harmful component, loss of data, communication line failure, unlawful third party
                       conduct, or theft, destruction, alteration or unauthorised access to records;</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo38'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(b)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>the
                       accuracy, suitability or currency of any information on the Website, the
                       Services, or any of its Services related products (including third party
                       material and advertisements on the Website);</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo38'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(c)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>costs
                       incurred as a result of you using the Website, the Services or any of the
                       products of Pharma Anywhere; and</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo38'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(d)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>the
                       Services or operation in respect to links which are provided for your
                       convenience.</p>

                   <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                               name=limitation-of-liability><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>10.<span
                                           style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span><![endif]>Limitation
                           of liability</a></h2>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo39'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>10.1.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Pharma Anywhere's total liability arising out of
                       or in connection with the Services or these Terms, however arising, including
                       under contract, tort (including negligence), in equity, under statute or
                       otherwise, will not exceed the resupply of the Services to you.</p>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo39'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>10.2.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>You expressly understand and agree that Pharma
                       Anywhere, its affiliates, employees, agents, contributors and licensors shall
                       not be liable to you for any direct, indirect, incidental, special
                       consequential or exemplary damages which may be incurred by you, however caused
                       and under any theory of liability. This shall include, but is not limited to,
                       any loss of profit (whether incurred directly or indirectly), any loss of
                       goodwill or business reputation and any other intangible loss.</p>

                   <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                               name=termination-of-contract><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>11.<span
                                           style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span><![endif]>Termination
                           of Contract</a></h2>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo40'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>11.1.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>The Terms will continue to apply until
                       terminated by either you or by Pharma Anywhere as set out below.</p>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo40'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>11.2.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>If you want to terminate the Terms, you may do
                       so by:</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo41'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(a)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>not
                       renewing the Subscription prior to the end of the Subscription Period;</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo41'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(b)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>providing
                       Pharma Anywhere with 28 days' notice of your intention to terminate; and</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo41'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(c)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>closing
                       your accounts for all of the services which you use, where Pharma Anywhere has
                       made this option available to you.</p>

                   <p class=MsoNormal style='margin-left:60.0pt;text-indent:-24.0pt;mso-list:l2 level2 lfo21;
tab-stops:list .5in'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'><span
                                       style='mso-spacerun:yes'> </span><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Your notice should be sent, in writing, to
                       Pharma Anywhere via the 'Contact Us' link on our homepage.</p>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo40'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>11.3.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Pharma Anywhere may at any time, terminate the
                       Terms with you if:</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo42'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(a)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>you
                       do not renew the Subscription at the end of the Subscription Period;</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo42'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(b)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>you
                       have breached any provision of the Terms or intend to breach any provision;</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo42'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(c)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Pharma
                       Anywhere is required to do so by law;</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo42'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(d)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>the
                       provision of the Services to you by Pharma Anywhere is, in the opinion of
                       Pharma Anywhere, no longer commercially viable.</p>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo40'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>11.4.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Subject to local applicable laws, Pharma
                       Anywhere reserves the right to discontinue or cancel your membership at any
                       time and may suspend or deny, in its sole discretion, your access to all or any
                       portion of the Website or the Services without notice if you breach any
                       provision of the Terms or any applicable law or if your conduct impacts Pharma Anywhere's
                       name or reputation or violates the rights of those of another party.</p>

                   <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                               name=indemnity><![if !supportLists]><span style='mso-fareast-font-family:Arial;
mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>12.<span
                                           style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span><![endif]>Indemnity</a></h2>

                   <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo43'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>12.1.<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>You agree to indemnify Pharma Anywhere, its
                       affiliates, employees, agents, contributors, third party content providers and
                       licensors from and against:</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo44'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(a)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>all
                       actions, suits, claims, demands, liabilities, costs, expenses, loss and damage
                       (including legal fees on a full indemnity basis) incurred, suffered or arising
                       out of or in connection with Your Content;</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo44'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(b)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>any
                       direct or indirect consequences of you accessing, using or transacting on the
                       Website or attempts to do so; and/or</p>

                   <p class=MsoNormal style='margin-left:85.05pt;text-indent:-24.95pt;mso-list:
l4 level3 lfo44'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>(c)<span
                                       style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>any
                       breach of the Terms.</p>

                   <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                               name=dispute-resolution><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>13.<span
                                           style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span><![endif]>Dispute
                           Resolution</a></h2>

                   <h3 style='margin-left:24.0pt;text-indent:-24.0pt;mso-list:l2 level1 lfo21;
tab-stops:list 0in'><a name=compulsory><![if !supportLists]><span
                                   style='mso-fareast-font-family:Arial;mso-bidi-font-family:Arial;font-weight:
normal;mso-bidi-font-weight:bold'><span style='mso-list:Ignore'><span
                                           style='mso-spacerun:yes'> </span><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>13.1. Compulsory:</a></h3>

                   <p class=MsoNormal style='margin-left:24.0pt;text-indent:-24.0pt;mso-list:l2 level1 lfo21;
tab-stops:list 0in'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'><span
                                       style='mso-spacerun:yes'> </span><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>If a dispute arises out of or relates to the
                       Terms, either party may not commence any Tribunal or Court proceedings in
                       relation to the dispute, unless the following clauses have been complied with
                       (except where urgent interlocutory relief is sought).</p>

                   <h3 style='margin-left:24.0pt;text-indent:-24.0pt;mso-list:l2 level1 lfo21;
tab-stops:list 0in'><a name=notice><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial;font-weight:normal;mso-bidi-font-weight:bold'><span
                                       style='mso-list:Ignore'><span style='mso-spacerun:yes'> </span><span
                                           style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>13.2. Notice:</a></h3>

                   <p class=MsoNormal style='margin-left:24.0pt;text-indent:-24.0pt;mso-list:l2 level1 lfo21;
tab-stops:list 0in'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'><span
                                       style='mso-spacerun:yes'> </span><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>A party to the Terms claiming a dispute ('<b
                               style='mso-bidi-font-weight:normal'>Dispute</b>') has arisen under the Terms,
                       must give written notice to the other party detailing the nature of the
                       dispute, the desired outcome and the action required to settle the Dispute.</p>

                   <h3 style='margin-left:24.0pt;text-indent:-24.0pt;mso-list:l2 level1 lfo21;
tab-stops:list 0in'><a name=resolution><![if !supportLists]><span
                                   style='mso-fareast-font-family:Arial;mso-bidi-font-family:Arial;font-weight:
normal;mso-bidi-font-weight:bold'><span style='mso-list:Ignore'><span
                                           style='mso-spacerun:yes'> </span><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>13.3. Resolution:</a></h3>

                   <p class=MsoNormal style='margin-left:24.0pt;text-indent:-24.0pt;mso-list:l2 level1 lfo21;
tab-stops:list 0in'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'><span
                                       style='mso-spacerun:yes'> </span><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>On receipt of that notice ('<b style='mso-bidi-font-weight:
normal'>Notice</b>') by that other party, the parties to the Terms ('<b
                               style='mso-bidi-font-weight:normal'>Parties</b>') must:</p>

                   <p class=MsoNormal style='margin-left:45.4pt;text-indent:-22.7pt;mso-list:l4 level2 lfo45'><![if !supportLists]><span
                               style='mso-fareast-font-family:Arial;mso-bidi-font-family:Arial;color:black'><span
                                   style='mso-list:Ignore'>1.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span><![endif]>Within 28 days of the Notice endeavour in good
                       faith to resolve the Dispute expeditiously by negotiation or such other means
                       upon which they may mutually agree;</p>

                   <p class=MsoNormal style='margin-left:45.4pt;text-indent:-22.7pt;mso-list:l4 level2 lfo45'><![if !supportLists]><span
                               style='mso-fareast-font-family:Arial;mso-bidi-font-family:Arial;color:black'><span
                                   style='mso-list:Ignore'>1.2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span><![endif]>If for any reason whatsoever, 28 days after the
                       date of the Notice, the Dispute has not been resolved, the Parties must either
                       agree upon selection of a mediator or request that an appropriate mediator be
                       appointed by the President of the Australian Mediation Association or his or
                       her nominee;</p>

                   <p class=MsoNormal style='margin-left:45.4pt;text-indent:-22.7pt;mso-list:l4 level2 lfo45'><![if !supportLists]><span
                               style='mso-fareast-font-family:Arial;mso-bidi-font-family:Arial;color:black'><span
                                   style='mso-list:Ignore'>1.3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span><![endif]>The Parties are equally liable for the fees and
                       reasonable expenses of a mediator and the cost of the venue of the mediation
                       and without limiting the foregoing undertake to pay any amounts requested by
                       the mediator as a pre-condition to the mediation commencing. The Parties must
                       each pay their own costs associated with the mediation;</p>

                   <p class=MsoNormal style='margin-left:45.4pt;text-indent:-22.7pt;mso-list:l4 level2 lfo45'><![if !supportLists]><span
                               style='mso-fareast-font-family:Arial;mso-bidi-font-family:Arial;color:black'><span
                                   style='mso-list:Ignore'>1.4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span><![endif]>The mediation will be held in The Gold Coast,
                       Australia.</p>

                   <h3 style='margin-left:24.0pt;text-indent:-24.0pt;mso-list:l2 level1 lfo21;
tab-stops:list 0in'><a name=confidential><![if !supportLists]><span
                                   style='mso-fareast-font-family:Arial;mso-bidi-font-family:Arial;font-weight:
normal;mso-bidi-font-weight:bold'><span style='mso-list:Ignore'><span
                                           style='mso-spacerun:yes'> </span><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>13.4. Confidential:</a></h3>

                   <p class=MsoNormal style='margin-left:24.0pt;text-indent:-24.0pt;mso-list:l2 level1 lfo21;
tab-stops:list 0in'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'><span
                                       style='mso-spacerun:yes'> </span><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>All communications concerning negotiations made
                       by the Parties arising out of and in connection with this dispute resolution
                       clause are confidential and to the extent possible, must be treated as
                       &quot;without prejudice&quot; negotiations for the purpose of applicable laws
                       of evidence.</p>

                   <h3 style='margin-left:24.0pt;text-indent:-24.0pt;mso-list:l2 level1 lfo21;
tab-stops:list 0in'><a name=termination-of-mediation><![if !supportLists]><span
                                   style='mso-fareast-font-family:Arial;mso-bidi-font-family:Arial;font-weight:
normal;mso-bidi-font-weight:bold'><span style='mso-list:Ignore'><span
                                           style='mso-spacerun:yes'> </span><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>13.5. Termination of Mediation:</a></h3>

                   <p class=MsoNormal style='margin-left:24.0pt;text-indent:-24.0pt;mso-list:l2 level1 lfo21;
tab-stops:list 0in'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'><span
                                       style='mso-spacerun:yes'> </span><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>If 3 months have elapsed after the start of a
                       mediation of the Dispute and the Dispute has not been resolved, either Party
                       may ask the mediator to terminate the mediation and the mediator must do so.</p>

                   <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                               name=venue-and-jurisdiction><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>14.<span
                                           style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span><![endif]>Venue
                           and Jurisdiction</a></h2>

                   <p class=MsoNormal style='margin-left:24.0pt;text-indent:-24.0pt;mso-list:l2 level1 lfo21;
tab-stops:list 0in'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'><span
                                       style='mso-spacerun:yes'> </span><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>The Services offered by Pharma Anywhere is
                       intended to be viewed by residents of Australia. In the event of any dispute
                       arising out of or in relation to the Website, you agree that the exclusive
                       venue for resolving any dispute shall be in the courts of New South Wales,
                       Australia.</p>

                   <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                               name=governing-law><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>15.<span
                                           style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span><![endif]>Governing
                           Law</a></h2>

                   <p class=MsoNormal style='margin-left:24.0pt;text-indent:-24.0pt;mso-list:l2 level1 lfo21;
tab-stops:list 0in'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'><span
                                       style='mso-spacerun:yes'> </span><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>The Terms are governed by the laws of New South
                       Wales, Australia. Any dispute, controversy, proceeding or claim of whatever
                       nature arising out of or in any way relating to the Terms and the rights
                       created hereby shall be governed, interpreted and construed by, under and
                       pursuant to the laws of New South Wales, Australia, without reference to
                       conflict of law principles, notwithstanding mandatory rules. The validity of
                       this governing law clause is not contested. The Terms shall be binding to the
                       benefit of the parties hereto and their successors and assigns.</p>

                   <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                               name=independent-legal-advice><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>16.<span
                                           style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span><![endif]>Independent
                           Legal Advice</a></h2>

                   <p class=MsoNormal style='margin-left:24.0pt;text-indent:-24.0pt;mso-list:l2 level1 lfo21;
tab-stops:list 0in'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'><span
                                       style='mso-spacerun:yes'> </span><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Both parties confirm and declare that the
                       provisions of the Terms are fair and reasonable and both parties having taken
                       the opportunity to obtain independent legal advice and declare the Terms are
                       not against public policy on the grounds of inequality or bargaining power or
                       general grounds of restraint of trade.</p>

                   <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                               name=severance><![if !supportLists]><span style='mso-fareast-font-family:Arial;
mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>17.<span
                                           style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span><![endif]>Severance</a></h2>

                   <p class=MsoNormal style='margin-left:24.0pt;text-indent:-24.0pt;mso-list:l2 level1 lfo21;
tab-stops:list 0in'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'><span
                                       style='mso-spacerun:yes'> </span><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>If any part of these Terms is found to be void
                       or unenforceable by a Court of competent jurisdiction, that part shall be
                       severed and the rest of the Terms shall remain in force.</p>

               </div>

            </div>
            <div class="modal-footer">
                <button type="button" id="savePolicyModal" data-url="{{ route('acceptTerms') }}" class="btn btn-primary">Accept</button>
                {{--<button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Close</button>--}}
            </div>

        </div>
    </div>
</div>