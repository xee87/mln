<div class="modal fade" id="privacyPolicyModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
    <div class="modal-dialog modal-lg modal- modal-dialog-centered modal-" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h3 class="modal-title" id="modal-title-default">Privacy Policy</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span style="font-size: 30px;color: #6652c0;" aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body">

                <div class="privacy-policy-content">

                <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                            name=we-respect-your-privacy><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>1.<span
                                        style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>We
                        respect your privacy</a></h2>

                <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo23'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>1.1.<span
                                    style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Pharma Anywhere respects your right to privacy
                    and is committed to safeguarding the privacy of our customers and website
                    visitors. We adhere to the Australian Privacy Principles contained in the <i
                            style='mso-bidi-font-style:normal'>Privacy Act</i> 1988 (Cth). This policy sets
                    out how we collect and treat your personal information.</p>

                <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo23'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>1.2.<span
                                    style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>&quot;Personal information&quot; is information
                    we hold which is identifiable as being about you.</p>

                <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                            name=collection-of-personal-information><![if !supportLists]><span
                                style='mso-fareast-font-family:Arial;mso-bidi-font-family:Arial'><span
                                    style='mso-list:Ignore'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Collection of personal information</a></h2>

                <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo24'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>2.1.<span
                                    style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Pharma Anywhere will, from time to time, receive
                    and store personal information you enter onto our website, provided to us
                    directly or given to us in other forms.</p>

                <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo24'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>2.2.<span
                                    style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>You may provide basic information such as your
                    name, phone number, address and email address to enable us to send information,
                    provide updates and process your product or service order. We may collect
                    additional information at other times, including but not limited to, when you
                    provide feedback, when you provide information about your personal or business
                    affairs, change your content or email preference, respond to surveys and/or
                    promotions, provide financial or credit card information, or communicate with
                    our customer support.</p>

                <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo24'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>2.3.<span
                                    style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Additionally, we may also collect any other
                    information you provide while interacting with us.</p>

                <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                            name=how-we-collect-your-personal-information><![if !supportLists]><span
                                style='mso-fareast-font-family:Arial;mso-bidi-font-family:Arial'><span
                                    style='mso-list:Ignore'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>How we collect your personal information</a></h2>

                <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo25'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>3.1.<span
                                    style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Pharma Anywhere collects personal information
                    from you in a variety of ways, including when you interact with us
                    electronically or in person, when you access our website and when we provide
                    our services to you. We may receive personal information from third parties. If
                    we do, we will protect it as set out in this Privacy Policy.</p>

                <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                            name=use-of-your-personal-information><![if !supportLists]><span
                                style='mso-fareast-font-family:Arial;mso-bidi-font-family:Arial'><span
                                    style='mso-list:Ignore'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Use of your personal information</a></h2>

                <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo26'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>4.1.<span
                                    style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Pharma Anywhere may use personal information
                    collected from you to provide you with information, updates and our services.
                    We may also make you aware of new and additional products, services and
                    opportunities available to you. We may use your personal information to improve
                    our products and services and better understand your needs.</p>

                <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo26'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>4.2.<span
                                    style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Pharma Anywhere may contact you by a variety of
                    measures including, but not limited to telephone, email, sms or mail.</p>

                <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                            name=disclosure-of-your-personal-information><![if !supportLists]><span
                                style='mso-fareast-font-family:Arial;mso-bidi-font-family:Arial'><span
                                    style='mso-list:Ignore'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Disclosure of your personal information</a></h2>

                <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo27'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>5.1.<span
                                    style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>We may disclose your personal information to any
                    of our employees, officers, insurers, professional advisers, agents, suppliers
                    or subcontractors insofar as reasonably necessary for the purposes set out in
                    this Policy. Personal information is only supplied to a third party when it is
                    required for the delivery of our services.</p>

                <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo27'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>5.2.<span
                                    style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>We may from time to time need to disclose
                    personal information to comply with a legal requirement, such as a law,
                    regulation, court order, subpoena, warrant, in the course of a legal proceeding
                    or in response to a law enforcement agency request.</p>

                <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo27'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>5.3.<span
                                    style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>We may also use your personal information to
                    protect the copyright, trademarks, legal rights, property or safety of Pharma
                    Anywhere, www.pharmaanywhere.com.au, its customers or third parties.</p>

                <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo27'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>5.4.<span
                                    style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Information that we collect may from time to
                    time be stored, processed in or transferred between parties located in
                    countries outside of Australia. These may include, but are not limited to
                    Australia, Pakistan, and USA.</p>

                <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo27'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>5.5.<span
                                    style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>If there is a change of control in our business
                    or a sale or transfer of business assets, we reserve the right to transfer to
                    the extent permissible at law our user databases, together with any personal
                    information and non-personal information contained in those databases. This
                    information may be disclosed to a potential purchaser under an agreement to
                    maintain confidentiality. We would seek to only disclose information in good faith
                    and where required by any of the above circumstances.</p>

                <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo27'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>5.6.<span
                                    style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>By providing us with personal information, you
                    consent to the terms of this Privacy Policy and the types of disclosure covered
                    by this Policy. Where we disclose your personal information to third parties,
                    we will request that the third party follow this Policy regarding handling your
                    personal information.</p>

                <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                            name=security-of-your-personal-information><![if !supportLists]><span
                                style='mso-fareast-font-family:Arial;mso-bidi-font-family:Arial'><span
                                    style='mso-list:Ignore'>6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Security of your personal information</a></h2>

                <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo28'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>6.1.<span
                                    style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Pharma Anywhere is committed to ensuring that
                    the information you provide to us is secure. In order to prevent unauthorised
                    access or disclosure, we have put in place suitable physical, electronic and
                    managerial procedures to safeguard and secure information and protect it from
                    misuse, interference, loss and unauthorised access, modification and
                    disclosure.</p>

                <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo28'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>6.2.<span
                                    style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>The transmission and exchange of information is
                    carried out at your own risk. We cannot guarantee the security of any
                    information that you transmit to us, or receive from us. Although we take
                    measures to safeguard against unauthorised disclosures of information, we
                    cannot assure you that personal information that we collect will not be
                    disclosed in a manner that is inconsistent with this Privacy Policy.</p>

                <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                            name=access-to-your-personal-information><![if !supportLists]><span
                                style='mso-fareast-font-family:Arial;mso-bidi-font-family:Arial'><span
                                    style='mso-list:Ignore'>7.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Access to your personal information</a></h2>

                <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo29'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>7.1.<span
                                    style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>You may request details of personal information
                    that we hold about you in accordance with the provisions of the <i
                            style='mso-bidi-font-style:normal'>Privacy Act</i> 1988 (Cth). A small
                    administrative fee may be payable for the provision of information. If you
                    would like a copy of the information, which we hold about you or believe that
                    any information we hold on you is inaccurate, out of date, incomplete,
                    irrelevant or misleading, please email us at support@pharmaanywhere.com.au.</p>

                <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo29'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>7.2.<span
                                    style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>We reserve the right to refuse to provide you
                    with information that we hold about you, in certain circumstances set out in
                    the Privacy Act.</p>

                <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                            name=complaints-about-privacy><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>8.<span
                                        style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Complaints
                        about privacy</a></h2>

                <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo30'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>8.1.<span
                                    style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>If you have any complaints about our privacy
                    practices, please feel free to send in details of your complaints to 39
                    Warrambool Road, Ocean Shores, New South Wales, 2483. We take complaints very
                    seriously and will respond shortly after receiving written notice of your
                    complaint.</p>

                <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                            name=changes-to-privacy-policy><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>9.<span
                                        style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]>Changes
                        to Privacy Policy</a></h2>

                <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo31'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>9.1.<span
                                    style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Please be aware that we may change this Privacy
                    Policy in the future. We may modify this Policy at any time, in our sole
                    discretion and all modifications will be effective immediately upon our posting
                    of the modifications on our website or notice board. Please check back from
                    time to time to review our Privacy Policy.</p>

                <h2 style='margin-left:.25in;text-indent:-.25in;mso-list:l0 level1 lfo22'><a
                            name=website><![if !supportLists]><span style='mso-fareast-font-family:Arial;
mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>10.<span
                                        style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></span><![endif]>Website</a></h2>

                <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo32'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>10.1.<span
                                    style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><i style='mso-bidi-font-style:normal'>When you
                        visit our website</i></p>

                <p class=MsoNormal style='margin-left:60.0pt;text-indent:-24.0pt;mso-list:l2 level2 lfo21;
tab-stops:list .5in'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'><span
                                    style='mso-spacerun:yes'> </span><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>When you come to our website
                    (www.pharmaanywhere.com.au) we may collect certain information such as browser
                    type, operating system, website visited immediately before coming to our site,
                    etc. This information is used in an aggregated manner to analyse how people use
                    our site, such that we can improve our service.</p>

                <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo32'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>10.2.<span
                                    style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><i style='mso-bidi-font-style:normal'>Cookies</i></p>

                <p class=MsoNormal style='margin-left:60.0pt;text-indent:-24.0pt;mso-list:l2 level2 lfo21;
tab-stops:list .5in'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'><span
                                    style='mso-spacerun:yes'> </span><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>We may from time to time use cookies on our
                    website. Cookies are very small files which a website uses to identify you when
                    you come back to the site and to store details about your use of the site. Cookies
                    are not malicious programs that access or damage your computer. Most web
                    browsers automatically accept cookies but you can choose to reject cookies by
                    changing your browser settings. However, this may prevent you from taking full
                    advantage of our website. Our website may from time to time use cookies to
                    analyses website traffic and help us provide a better website visitor
                    experience. In addition, cookies may be used to serve relevant ads to website
                    visitors through third party services such as Google Adwords. These ads may
                    appear on this website or other websites you visit.</p>

                <p class=MsoNormal style='margin-left:60.1pt;text-indent:-42.25pt;mso-list:
l0 level2 lfo32'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'>10.3.<span
                                    style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]><i style='mso-bidi-font-style:normal'>Third
                        party sites</i></p>

                <p class=MsoNormal style='margin-left:60.0pt;text-indent:-24.0pt;mso-list:l2 level2 lfo21;
tab-stops:list .5in'><![if !supportLists]><span style='mso-fareast-font-family:
Arial;mso-bidi-font-family:Arial'><span style='mso-list:Ignore'><span
                                    style='mso-spacerun:yes'> </span><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><![endif]>Our site may from time to time have links to
                    other websites not owned or controlled by us. These links are meant for your
                    convenience only. Links to third party websites do not constitute sponsorship
                    or endorsement or approval of these websites. Please be aware that Pharma
                    Anywhere is not responsible for the privacy practises of other such websites.
                    We encourage our users to be aware, when they leave our website, to read the
                    privacy statements of each and every website that collects personal
                    identifiable information.</p>
                </div>

            </div>

        </div>
        <div class="modal-footer d-none">
            <button type="button" data-dismiss="modal" class="btn btn-primary">Okay</button>
            {{--<button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Close</button>--}}
        </div>

    </div>
</div>
</div>