<div class="modal fade" id="verifyNumber" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Verify Number</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="verifyCode" action="{{ route('verifyphone.verify') }}" method="post">
            <div class="modal-body">
                <div class="row">

                    <div class="col-md-12">
                        <div class="form-group{{ $errors->has('verification_code') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-email">{{ __('Code') }}</label>
                            <div class="input-group input-group-alternative mb-3">
                                <input class="form-control{{ $errors->has('verification_code') ? ' is-invalid' : '' }}" placeholder="{{ __('Enter Code') }}" type="number" name="verification_code" value="{{ old('verification_code') }}" required autofocus>
                            </div>
                            @if ($errors->has('verification_code'))
                                <span class="invalid-feedback" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('verification_code') }}</strong>
                                    </span>
                            @endif
                            <input type="hidden" name="user_id" value="{{ isset($user->id) ? $user->id : '' }}">
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
{{--                <a href="{{ route('register.message', ['id' => $user->id]) }}" type="button" class="btn btn-secondary" >No</a>--}}
                <button type="submit" class="btn btn-primary submitVCode" >Verify</button>
            </div>
            </form>
        </div>
    </div>
</div>
