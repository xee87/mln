@extends('frontend-layouts.layout')
@section('page-title', 'About Us')
@section('content')
    <div class="main-content-top">
        <div class="main-wrapper">
            <div class="row">
                <div class="large-6 columns">
                    <h2>About Us</h2>
                </div>
                <div class="large-6 columns">
                    <ul class="breadcrumbs right">
                        <li>You are here:</li>
                        <li><a href="#">home</a></li>
                        <li><a href="#">About us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="main-wrapper">
        <!-- Main Content -->
        <div class="row main-content">
            <div class="large-12 columns">
                <div class="row">
                    <div class="large-8 columns">
                        <h3>Who we are</h3>

                        <div class="divider"><span></span></div>
                        <img src="{{asset('argon/img/brand/who-we-are.png')}}" alt="post-img" class="alignleft">

                        <p>
                            This is <a href="#">Smartcuts </a> of Lorem Ipsum. Proin gravida nibh vel velit
                            auctor aliquet. Aenean
                            sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh
                            id elit. Duis sed odio
                            sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus
                            a odio tincidunt auctor
                            a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti
                            sociosqu ad litora torquent
                            per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis
                            dapibus condimentum sit amet
                            a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam
                            pharetra, erat sed fermentum
                            feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci
                            enim.This is Photoshop's version
                            of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
                        </p>
                    </div>

                    <div class="large-4 columns">
                        <h3>Some Statistics</h3>

                        <div class="divider"><span></span></div>
                        <p class="client-statistics">Satisfied Clients:</p>

                        <div class="figures">6549</div>
                        <p class="client-statistics">Succesful Surgeries</p>

                        <div class="figures">1329</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="large-12 columns service-block">
                <div class="row">
                    <div class="large-6 columns">
                        <h5>OUR Services</h5>

                        <div class="divider"><span></span></div>
                        <ul id="example1" class="accordion">
                            <li>
                                <div class="handle"><span><i></i></span><a href="#">Teeth whitening</a></div>
                                <div class="panel loading">
                                    <p>Tartar: A common term for dental calculus, a hard deposit that adheres to teeth
                                        and produces a rough surface that attracts plaque.</p>
                                </div>
                            </li>
                            <li>
                                <div class="handle"><span><i></i></span>Crown dental bridges</div>
                                <ul class="panel loading">
                                    <li>How about&hellip;</li>
                                    <li>&hellip; a list &hellip;</li>
                                    <li>&hellip; of items?</li>
                                </ul>
                            </li>
                            <li>
                                <div class="handle"><span><i></i></span>Rehabilitation center</div>
                                <p class="panel loading">
                                    An image in a paragraph.
                                </p>
                            </li>
                        </ul>
                    </div>
                    <div class="large-6 columns">
                        <div class="title-block">
                            <h5>Smartcuts </h5>

                            <div class="divider"><span></span></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="module_content testimonial-block">
                            <ul id="testimonial_slide">
                                <li>
                                    <div class="testimonial-content">

                                        <p>

                                        <blockquote>
                                                  {{-- <a href=""> Smartcuts </a>--}} Most "innovation" inside industries and companies today focuses on making faster horses, not automobiles.
                                        </blockquote>
                                        </p>
                                    </div>
                                    <span class="testimonial-divider"></span>

                                    <div class="testimonial-meta"><cite>Shane Snow</cite> - {{--<a href="mail:getnugget.co">getnugget.co </a>--}}</div>

                                </li>
                                <li>
                                    <div class="testimonial-content">
                                        <p>
                                        <blockquote>Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum,
                                            nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus
                                            a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio
                                            tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor
                                            eu in elit.
                                            </blockquote>
                                        </p>
                                    </div>
                                    <span class="testimonial-divider"></span>

                                    <div class="testimonial-meta"><cite>Faton Avdiu</cite> - Web Designer</div>

                                </li>
                            </ul>
                            <div class="clearfix"></div>
                            <a class="prev" id="slide_prev1" href="#"><img
                                        src="{{asset('argon/img/brand/arrow_left.png')}}" alt="Previous"/></a>
                            <a class="next" id="slide_next1" href="#"><img
                                        src="{{asset('argon/img/brand/arrow_right.png')}}" alt="Next"/></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="large-12 columns">
                <div class="title-block">
                    <h3>OUR DOCTORS</h3>
                </div>
                <div class="divider"><span></span></div>
                <div class="work_slide">
                    <ul id="work_slide">
                        <li>
                            <div class="view view-two">
                                <img src="{{asset('argon/img/brand/we.jpg')}}" alt=""/>

                                <div class="mask">
                                    <h3>Specialties</h3>

                                    <p>Colon and rectal surgery, Dental surgery, Oral and maxillofacial surgery.</p>
                                    <a class="button btn-icon icon-2" href="portfolio-single.html"><i
                                                class="icon-external-link icon-large"></i></a>
                                </div>
                            </div>
                            <div class="doctor-name">John Smith</div>
                        </li>
                        <li>
                            <div class="view view-two">
                                <img src="{{asset('argon/img/brand/2asdf.jpg')}}" alt=""/>

                                <div class="mask">
                                    <h3>Specialties</h3>

                                    <p>Pediatric surgery, Plastic surgery, Podiatric surgery.</p>
                                    <a class="button btn-icon icon-2" href="#"><i
                                                class="icon-external-link icon-large"></i></a>
                                </div>
                            </div>
                            <div class="doctor-name">Adriana Lima</div>
                        </li>
                        <li>
                            <div class="view view-two">
                                <img src="{{asset('argon/img/brand/3das.jpg')}}" alt=""/>

                                <div class="mask">
                                    <h3>Specialties</h3>

                                    <p>Trauma surgery, Thoracic surgery, Urology, Veterinary surgery.</p>
                                    <a class="button btn-icon icon-2" href="#"><i
                                                class="icon-external-link icon-large"></i></a>
                                </div>
                            </div>
                            <div class="doctor-name">M.D. House</div>
                        </li>
                        <li>
                            <div class="view view-two">
                                <img src="{{asset('argon/img/brand/dwe3.jpg')}}" alt=""/>

                                <div class="mask">
                                    <h3>Specialties</h3>

                                    <p>All in one master, the genius, the man behind the curtain.</p>
                                    <a class="button btn-icon icon-2" href="#"><i
                                                class="icon-external-link icon-large"></i></a>
                                </div>
                            </div>
                            <div class="doctor-name">Lonely Shepherd</div>
                        </li>
                        <li>
                            <div class="view view-two">
                                <img src="{{asset('argon/img/brand/4gr.jpg')}}" alt=""/>

                                <div class="mask">
                                    <h3>Specialties</h3>

                                    <p>Aenean eget mauris nibh, eu pellentesque ipsum. The lorem is totally site
                                        amet.</p>
                                    <a class="button btn-icon icon-2" href="#"><i
                                                class="icon-external-link icon-large"></i></a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="view view-two">
                                <img src="{{asset('argon/img/brand/3das.jpg')}}" alt=""/>

                                <div class="mask">
                                    <h3>Specialties</h3>

                                    <p>Aenean eget mauris nibh, eu pellentesque ipsum. The lorem is totally site
                                        amet.</p>
                                    <a class="button btn-icon" href="#"><i class="icon-zoom-in icon-large"></i></a>
                                </div>
                            </div>
                            <div class="doctor-name">Enzo Ferrari</div>
                        </li>
                    </ul>

                    <div class="clearfix"></div>
                    <a class="prev" id="slide_prev" href="#"><img src="{{asset('argon/img/brand/arrow_left.png')}}"
                                                                  alt="Previous"/></a>
                    <a class="next" id="slide_next" href="#"><img src="{{asset('argon/img/brand/arrow_right.png')}}"
                                                                  alt="Next"/></a>
                </div>
            </div>
        </div>
    </div>
@endsection
