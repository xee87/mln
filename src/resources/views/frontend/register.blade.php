@extends('frontend-layouts.layout')
@section('page-title', 'Registration')
@section('content')
        <!-- SIGN UP -->
<div class="main-content-top">
    <div class="main-wrapper">
        <div class="row">
            <div class="large-6 columns">
                <h2>Sign Up</h2>
            </div>
            <div class="large-6 columns">
                <ul class="breadcrumbs right">
                    <li>You are here:</li>
                    <li><a href="#">home</a></li>
                    <li><a href="#">Sign Up</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="main-wrapper">
    <div class="content_wrapper">
        <div class="row">
            <div class="large-12 columns">
                <h3 class="contact_title">Sign Up</h3>

                <div class="divider"><span></span></div>
                <div id="status"></div>
                <div class="contact_form">
                    <div class="row">
                        <form id="appointment-contact-form" role="form" action="{{ route('register') }}" method="POST" class="contactForm">
                           @csrf
                            <div class="large-12 columns">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback color-theme" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Email" value="{{ old('email') }}" id="contactemail" required />

                            </div>
                            <div class="large-12 columns">
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback color-theme" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" name="password" placeholder="Password" id="contactPassword"/>

                            </div>
                            <div class="large-12 columns">
                                <input type="password" name="password_confirmation" placeholder="Confirm Password" id="contactConfirmPassword"/>
                            </div>
                            <div class="small-4 columns">
                                <input type="submit" class="button right" value="Sign Up" name="send" id="send"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="large-6 columns">
                <h3 class="contact_title">Pharmacy</h3>

                <div class="divider"><span></span></div>
                <div class="contact_info">
                    <ul class="about-info garnik">
                        <li><i class="icon-home"></i><span>lorem ipsum street</span></li>
                        <li><i class="icon-phone"></i><span>+399 (500) 321 9548</span></li>
                        <li><i class="icon-envelope"></i><a href="mailto:info@Medico.com">info@Medico.com</a></li>
                    </ul>
                </div>
            </div>
            <div class="large-6 columns">

                <h3 class="contact_title">Where to find us</h3>

                <div class="divider"><span></span></div>
                <p>Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id
                    elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.
                    Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu
                    in elit. Clatss aptent taciti sociosqu ad litora </p>
            </div>
        </div>
    </div>
</div>

@endsection
