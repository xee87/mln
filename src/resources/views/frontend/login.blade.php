@extends('frontend-layouts.layout')
@section('page-title', 'Login')
@section('content')
        <!-- LOGIN -->
<div class="main-content-top">
    <div class="main-wrapper">
        <div class="row">
            <div class="large-6 columns">
                <h2>Login</h2>
            </div>
            <div class="large-6 columns">
                <ul class="breadcrumbs right">
                    <li>You are here:</li>
                    <li><a href="#">home</a></li>
                    <li><a href="#">Login</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="main-wrapper main-wrapper-login">
    <div class="content_wrapper">
        <div class="row">
            <div class="large-12 columns">
                <h3 class="contact_title">Login</h3>

                <div class="divider"><span></span></div>
                <div id="status"></div>
                <div class="contact_form">
                    <div class="small-12">
                        @if (session('status'))
                            <p class="success-message">{{ session('status') }}</p>
                        @endif
                    </div>
                    <div class="row">
                        <form id="appointment-contact-form" action="{{ route('login') }}" method="POST" class="contactForm">
                            @csrf
                            <div class="large-12 columns">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback color-theme" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" type="email" name="email" placeholder="Email" value="{{ old('email') }}" id="contactemail" required>
                            </div>
                            <div class="large-12 columns">
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback color-theme" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="{{ __('Password') }}" type="password" required id="contactPassword">

                            </div>
                            <div class="large-12 columns">
                                <label for="remember-me" class="">
                                    <span>Remember me</span>
                                    &nbsp;<span>
                                        <input class="custom-control-input" name="remember" id="customCheckLogin" type="checkbox" {{ old('remember') ? 'checked' : '' }}>
                                    </span>
                                </label>
                            </div>
                            <div class="small-4 columns">
                                <input type="submit" class="button right" value="Login" name="send" id="send">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>







@endsection
