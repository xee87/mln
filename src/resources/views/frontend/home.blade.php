@extends('frontend-layouts.layout')
@section('page-title', 'Home')
@section('content')

    <div class="intro-video">
        <video autoplay muted loop id="myVideo" width="100%" height="500">
            <source src="{{asset('/argon/img/brand/Order-g-animation.mp4')}} " type="video/mp4" title="#caption1">
        </video>
    </div>

   {{-- <div class="slider-wrapper theme-default">
        <div id="slider" class="nivoSlider">
            <video autoplay muted loop id="myVideo" width="100%" height="400">
                <source src="{{asset('/argon/img/brand/Order-g-animation.mp4')}} " type="video/mp4" title="#caption1">
            </video>
        </div>--}}
        {{-- <video autoplay muted loop id="myVideo">

                <source src="{{asset('/argon/img/brand/Order-g-animation.mp4')}} " type="video/mp4" title="#caption1">


          </video>--}}
        {{--<video autoplay muted loop id="myVideo">
        <source src="{{asset('/argon/img/brand/Order-g-animation.mp4')}}" data-thumb="images/demo/news/1.jpg" alt="" title="#caption1"/>
        --}}{{--<a href="http://www.google.com">
            <img src="{{asset('/argon/img/brand/Order-g-animation.mp4')}}" data-thumb="images/demo/news/2.jpg" alt="" title="#caption2"/>
        </a>
        <img src="{{asset('/argon/img/brand/Order-g-animation.mp4')}}" data-thumb="images/demo/news/3.jpg" alt="" title="#caption3"/>--}}{{--
        </video>--}}
        {{--  </div>--}}
        {{--<div id="caption1" class="nivo-html-caption">
            <p class="nivotitle v1">Medicine definition</p>

            <p class="nivotitle v2">Applied science or practice of the diagnosis, treatment, and prevention of
                disease</p>

            <p class="nivotitle v3">Built on foundation 4</p>
        </div>
        <div id="caption2" class="nivo-html-caption">
            <p class="nivotitle v1">Dynamic contact form</p>

            <p class="nivotitle v2">Documentation</p>

            <p class="nivotitle v3">Easy to use and abuse</p>
        </div>
        <div id="caption3" class="nivo-html-caption">
            <p class="nivotitle v1">Fully responsive</p>

            <p class="nivotitle v2">Lots of shortcodes</p>

            <p class="nivotitle v3">Built on foundation 4</p>
        </div>--}}

        <div class="main-wrapper app-wrapper">
            <div class="appointment-block grey-bg">
                <div class="row">
                    <div class="large-3 columns red">
                        <p>Make an Appoinment</p>
                        &nbsp;<!-- Put appointemnt label here -->
                    </div>
                    <div class="large-9 columns">
                        <form method="POST" action="#" id="appointment-contact-form">
                            <div class="row">
                                <div class="large-3 columns">
                                    <input type="text" placeholder="Full Name" name="name"/>
                                    <input type="text" placeholder="Phone Nomber" name="name"/>
                                </div>
                                <div class="large-3 columns">
                                    <input type="text" placeholder="E-mail Address" name="name"/>
                                    <input class="datepicker" type="text" placeholder="Appointment Date" name="name"/>
                                </div>
                                <div class="large-3 columns">
                                    <textarea cols="10" rows="15" name="message" placeholder="Message"></textarea>
                                </div>
                                <div class="large-3 columns">
                                    <input type="submit" class="blue button radius" value="Make Appointment"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- End Main Slider -->
    <!-- Main Content -->
    <div class="main-wrapper bg-wrapper">
        <div class="row main-content">
            <div class="large-12 columns">
                <div class="row">
                    <div class="large-3 columns">
                        <div class="featured-block">
                            <a href="#">
                                <span class="icon-sitemap fblock-icon"></span>

                                <div class="fblock-content">
                                    <p class="fblock-main">Exceptional <strong>Service</strong></p>

                                    <p class="fblock-sub">Learn more</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="large-3 columns">
                        <div class="featured-block">
                            <a href="#">
                                <span class="icon-lightbulb fblock-icon"></span>

                                <div class="fblock-content">
                                    <p class="fblock-main">Amazing <strong>Design</strong></p>

                                    <p class="fblock-sub">Learn more</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="large-3 columns">
                        <div class="featured-block">
                            <a href="#">
                                <span class="icon-laptop fblock-icon"></span>

                                <div class="fblock-content">
                                    <p class="fblock-main">Fully <strong>Responsive</strong></p>

                                    <p class="fblock-sub">Learn more</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="large-3 columns">
                        <div class="featured-block">
                            <a href="#">
                                <span class="icon-cogs fblock-icon"></span>

                                <div class="fblock-content">
                                    <p class="fblock-main">Easy <strong>Customization</strong></p>

                                    <p class="fblock-sub">Learn more</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="large-12 columns">
                <div class="title-block">
                    <h3>Our Doctors</h3>

                    <div class="divider"><span></span></div>
                </div>
                <div class="work_slide">
                    <ul id="work_slide">
                        <li>
                            <div class="view view-two">
                                <img src="{{asset('argon/img/brand/we.jpg')}}" alt=""/>

                                <div class="mask">
                                    <h3>Specialties</h3>

                                    <p>Colon and rectal surgery, Dental surgery, Oral and maxillofacial surgery.</p>
                                    <a class="button btn-icon icon-2" href="portfolio-single.html"><i
                                                class="icon-external-link icon-large"></i></a>
                                </div>
                            </div>
                            <div class="doctor-name">John Smith</div>
                        </li>
                        <li>
                            <div class="view view-two">
                                <img src="{{asset('argon/img/brand/2asdf.jpg')}}" alt=""/>

                                <div class="mask">
                                    <h3>Specialties</h3>

                                    <p>Pediatric surgery, Plastic surgery, Podiatric surgery.</p>
                                    <a class="button btn-icon icon-2" href="#"><i
                                                class="icon-external-link icon-large"></i></a>
                                </div>
                            </div>
                            <div class="doctor-name">Adriana Lima</div>
                        </li>
                        <li>
                            <div class="view view-two">
                                <img src="{{asset('argon/img/brand/3das.jpg')}}" alt=""/>

                                <div class="mask">
                                    <h3>Specialties</h3>

                                    <p>Trauma surgery, Thoracic surgery, Urology, Veterinary surgery.</p>
                                    <a class="button btn-icon icon-2" href="#"><i
                                                class="icon-external-link icon-large"></i></a>
                                </div>
                            </div>
                            <div class="doctor-name">M.D. House</div>
                        </li>
                        <li>
                            <div class="view view-two">
                                <img src="{{asset('argon/img/brand/jfjg1.jpg')}}" alt=""/>

                                <div class="mask">
                                    <h3>Specialties</h3>

                                    <p>All in one master, the genius, the man behind the curtain.</p>
                                    <a class="button btn-icon icon-2" href="#"><i
                                                class="icon-external-link icon-large"></i></a>
                                </div>
                            </div>
                            <div class="doctor-name">Lonely Shepherd</div>
                        </li>
                        <li>
                            <div class="view view-two">
                                <img src="{{asset('argon/img/brand/cx2.jpg')}}" alt=""/>

                                <div class="mask">
                                    <h3>Specialties</h3>

                                    <p>Aenean eget mauris nibh, eu pellentesque ipsum. The lorem is totally site
                                        amet.</p>
                                    <a class="button btn-icon icon-2" href="#">
                                        <i class="icon-external-link icon-large"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="doctor-name">Lisa Minelly</div>
                        </li>
                        <li>
                            <div class="view view-two">
                                <img src="{{asset('argon/img/brand/dwe3.jpg')}}" alt=""/>

                                <div class="mask">
                                    <h3>Specialties</h3>

                                    <p>Aenean eget mauris nibh, eu pellentesque ipsum. The lorem is totally site
                                        amet.</p>
                                    <a class="button btn-icon" href="#"><i class="icon-zoom-in icon-large"></i></a>
                                </div>
                            </div>
                            <div class="doctor-name">Enzo Ferrari</div>
                        </li>
                    </ul>

                    <div class="clearfix"></div>
                    <a class="prev" id="slide_prev" href="#"><img src="{{asset('argon/img/brand/arrow_left.png')}}"
                                                                  alt="Previous"/></a>
                    <a class="next" id="slide_next" href="#"><img src="{{asset('argon/img/brand/arrow_right.png')}}"
                                                                  alt="Next"/></a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="large-12 columns">
                <div class="title-block">
                    <h3>Latest News</h3>

                    <div class="clearfix"></div>
                </div>
                <div class="divider"><span></span></div>
                <div class="row">
                    <article class="large-3 columns">
                        <div class="mod_con_img">
                            <img src="{{asset('argon/img/brand/jfjg1.jpg')}}" alt=""/>
                            <ul class="meta">
                                <li><i class="icon-time"></i>Apr 22, 2013</li>
                            </ul>
                        </div>
                        <div class="mod_con_text">
                            <h5>Will 2013 be the year of the smartwatch?</h5>

                            <div class="divline"><span></span></div>
                            <p>Lorem quis bibendum auctor, nisi elit consequat ipsum, nec </p>
                            <a href="#" class="button">Read More</a>
                        </div>
                    </article>
                    <article class="large-3 columns">
                        <div class="mod_con_img">
                            <img src="{{asset('argon/img/brand/cx2.jpg')}}" alt=""/>
                            <ul class="meta">
                                <li><i class="icon-time"></i>Apr 22, 2013</li>
                            </ul>
                        </div>

                        <div class="mod_con_text">
                            <h5>Will 2013 be the year of the smartwatch?</h5>

                            <div class="divline"><span></span></div>
                            <p>Lorem quis bibendum auctor, nisi elit consequat ipsum, nec </p>
                            <a href="#" class="button">Read More</a>
                        </div>
                    </article>
                    <article class="large-3 columns">
                        <div class="mod_con_img">
                            <img src="{{asset('argon/img/brand/dwe3.jpg')}}" alt=""/>
                            <ul class="meta">
                                <li><i class="icon-time"></i>Apr 22, 2013</li>
                            </ul>
                        </div>

                        <div class="mod_con_text">
                            <h5>Will 2013 be the year of the smartwatch?</h5>

                            <div class="divline"><span></span></div>
                            <p>Lorem quis bibendum auctor, nisi elit consequat ipsum, nec </p>
                            <a href="#" class="button">Read More</a>
                        </div>
                    </article>
                    <article class="large-3 columns">
                        <div class="mod_con_img">
                            <img src="{{asset('argon/img/brand/4lkl.jpg')}}" alt=""/>
                            <ul class="meta">
                                <li><i class="icon-time"></i>Apr 22, 2013</li>
                            </ul>
                        </div>
                        <div class="mod_con_text">
                            <h5>Will 2013 be the year of the smartwatch?</h5>

                            <div class="divline"><span></span></div>
                            <p>Lorem quis bibendum auctor, nisi elit consequat ipsum, nec </p>
                            <a href="#" class="button">Read More</a>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
    <!-- end main content-->

@endsection
