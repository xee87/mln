@extends('frontend-layouts.layout')
@section('page-title', 'Registration Details')
@section('content')
    <!-- SIGN UP -->
    <div class="main-content-top">
        <div class="main-wrapper">
            <div class="row">
                <div class="large-6 columns">
                    <h2>Sign Up Details</h2>
                </div>
                <div class="large-6 columns">
                    <ul class="breadcrumbs right">
                        <li>You are here:</li>
                        <li><a href="#">home</a></li>
                        <li><a href="#">Sign Up</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="container mt--8 pb-5">
        <!-- Table -->
        <div class="row justify-content-center">
            <div class="col-lg-10 col-md-10">
                <div class="card bg-secondary shadow border-0">

                    <div class="card-body px-lg-5 py-lg-5">

                        <form role="form" method="POST" action="{{ route('register.info.store') }}">
                            @csrf
                            <input type="hidden" name="user_id" value="{{ $user->id }}">
                            <input type="hidden" name="status" value="1">
                            <div class="row">

                                <div class="large-6 columns">

                                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-email">{{ __('Pharmacist Name') }}</label>
                                        <div class="input-group input-group-alternative mb-3">
                                            <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Pharmacist Name') }}" type="text" name="name" value="{{ old('name') }}" required autofocus>
                                        </div>
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                </div>

                                <div class="large-6 columns">

                                    <div class="form-group{{ $errors->has('pharmacy_name') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-email">{{ __('Pharmacy Name') }}</label>
                                        <div class="input-group input-group-alternative mb-3">
                                            <input class="form-control{{ $errors->has('pharmacy_name') ? ' is-invalid' : '' }}" placeholder="{{ __('Pharmacy Name') }}" type="text" name="pharmacy_name" value="{{ old('pharmacy_name') }}" required autofocus>
                                        </div>
                                        @if ($errors->has('pharmacy_name'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('pharmacy_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                </div>

                                <div class="large-6 columns">

                                    <div class="form-group{{ $errors->has('pharmacy_email') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-email">{{ __('Pharmacy Email') }}</label>
                                        <div class="input-group input-group-alternative mb-3">
                                            <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Pharmacy Email') }}" type="email" name="pharmacy_email" value="{{ $user->email }}" readonly autofocus>
                                        </div>
                                        @if ($errors->has('pharmacy_email'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('pharmacy_email') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                </div>

                                <div class="large-6 columns">

                                    <div class="form-group{{ $errors->has('pharmacy_phone') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-email">{{ __('Pharmacy Phone') }}</label>
                                        <div class="input-group input-group-alternative mb-3">
                                            <input class="form-control{{ $errors->has('pharmacy_phone') ? ' is-invalid' : '' }}" placeholder="{{ __('Pharmacy Phone') }}" type="text" name="pharmacy_phone" value="{{ old('pharmacy_phone') }}" required autofocus>
                                        </div>
                                        @if ($errors->has('pharmacy_phone'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('pharmacy_phone') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                </div>

                                <div class="large-6 columns">
                                    <div class="form-group{{ $errors->has('address') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-email">{{ __('Address') }}</label>
                                        <div class="input-group input-group-alternative mb-3">
                                            <input id="location" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" placeholder="{{ __('Address') }}" type="text" name="address" value="{{ old('address') }}" required autofocus>
                                        </div>
                                        @if ($errors->has('address'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="large-6 columns">

                                    <div class="form-group{{ $errors->has('role') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-email">{{ __('Role') }}</label>
                                        <div class="input-group input-group-alternative mb-3">
                                            <input class="form-control{{ $errors->has('role') ? ' is-invalid' : '' }}" placeholder="{{ __('Role') }}" type="text" name="role" value="{{ old('role') }}"
                                                   required autofocus>
                                        </div>
                                        @if ($errors->has('role'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                </div>

                                <div class="large-6 columns">

                                    <div class="form-group">
                                        <label class="form-control-label" for="input-email">{{ __('PBS Status') }}</label><br>

                                        <div class="large-4 columns">
                                            <input type="radio" id="yes-pbs" name="pbs" checked value="1">
                                            <label style="margin-right: 10px;" for="male">PBS APPROVED</label>
                                        </div>

                                        <div class="large-4 columns">
                                            <input type="radio" id="no-pbs" name="pbs" value="0">
                                            <label for="female">NOT PBS APPROVED</label><br>
                                        </div>
                                        <div class="large-4 columns"></div>
                                    </div>

                                </div>

                                <div class="large-6 columns">

                                    <div id="pbs-number" class="form-group{{ $errors->has('pbs_number') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-email">{{ __('PBS Approval number') }}</label>
                                        <div class="input-group input-group-alternative mb-3">
                                            <input class="form-control{{ $errors->has('role') ? ' is-invalid' : '' }}" placeholder="{{ __('PBS Approval number') }}" type="text" name="pbs_number" value="{{ old('pbs_number') }}"  autofocus>
                                        </div>
                                        @if ($errors->has('pbs_number'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                                 <strong>{{ $errors->first('pbs_number') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-primary mt-4">{{ __('Create account') }}</button>
                            </div>
                            <div data-cancelUrl="{{ route('register.message', ['id' => $user->id]) }}" id="confirm-wholesaler"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


