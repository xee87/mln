@extends('frontend-layouts.layout')
@section('page-title', 'Message')
@section('content')
        <!-- LOGIN -->

<div class="main-wrapper main-wrapper-login">
    <div class="content_wrapper">
        <div class="row">
            <div class="large-12 columns">
                <h3 class="contact_title">THANK YOU FOR CONNECT!</h3>

                <div class="divider"><span></span></div>
                <div id="status"></div>
                <div style="min-height: 250px" class="contact_form">
                    @if (isset($message))
                        {{$message}}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>







@endsection
