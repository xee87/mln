
@php($days = \App\Helper\PA::trialNotify())

@if($days)

<div class="col-12">
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <span class="alert-inner--icon"><i class="ni ni-bell-55"></i></span>
        <span class="alert-inner--text">
            <strong>Warning!</strong>
            {{ $days }} days remaining in your availing service. To continue without intruption
Please upgrade your account by clicking <a class="text-white" href="#"><i>here</i></a>.
        </span>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
</div>

@endif