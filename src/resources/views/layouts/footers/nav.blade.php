<div class="row align-items-center justify-content-xl-between">
    <div class="col-xl-12">
        <div class="copyright text-center text-xl-center text-muted">
            &copy; {{ now()->year }}
            <a href="{{ env('APP_URL') }}" class="font-weight-bold ml-1">{{ env('APP_NAME') }}.</a>
            All Rights Reserved. <br>
            <a href="javascript:void(0)" class=" font-weight-bold ml-1" data-toggle="modal" data-target="#privacyPolicyModal">Privacy Policy</a>
        </div>
    </div>
    {{--<div class="col-xl-6">--}}
        {{--<ul class="nav nav-footer justify-content-center justify-content-xl-end">--}}
            {{--<li class="nav-item">--}}
                {{--<a href="https://www.creative-tim.com" class="nav-link" target="_blank">Creative Tim</a>--}}
            {{--</li>--}}
            {{--<li class="nav-item">--}}
                {{--<a href="https://www.updivision.com" class="nav-link" target="_blank">Updivision</a>--}}
            {{--</li>--}}
            {{--<li class="nav-item">--}}
                {{--<a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>--}}
            {{--</li>--}}
            {{--<li class="nav-item">--}}
                {{--<a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>--}}
            {{--</li>--}}
            {{--<li class="nav-item">--}}
                {{--<a href="https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md" class="nav-link" target="_blank">MIT License</a>--}}
            {{--</li>--}}
        {{--</ul>--}}
    {{--</div>--}}
</div>