<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand pt-0" href="{{ route('home') }}">
            <img style="max-height: 6.5rem;" src="{{ asset('argon') }}/img/brand/pa.png" class="navbar-brand-img" alt="...">
        </a>
        <!-- User -->
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-circle">
                        <img alt="Image placeholder" src="{{ asset('argon') }}/img/theme/team-1-800x800.jpg">
                        </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">{{ __('Welcome!') }}</h6>
                    </div>
                    <a href="{{ route('profile.edit') }}" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span>{{ __('My profile') }}</span>
                    </a>
                    <a href="{{ route('settings') }}" class="dropdown-item">
                        <i class="ni ni-settings-gear-65"></i>
                        <span>{{ __('Settings') }}</span>
                    </a>

                    <div class="dropdown-divider"></div>
                    <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        <i class="ni ni-user-run"></i>
                        <span>{{ __('Logout') }}</span>
                    </a>
                </div>
            </li>
        </ul>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset('argon') }}/img/brand/pa.png">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- Form -->
            <form class="mt-4 mb-3 d-md-none">
                <div class="input-group input-group-rounded input-group-merge">
                    <input type="search" class="form-control form-control-rounded form-control-prepended" placeholder="{{ __('Search') }}" aria-label="Search">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <span class="fa fa-search"></span>
                        </div>
                    </div>
                </div>
            </form>
            <!-- Navigation -->
            <ul class="navbar-nav">


                @role('Admin')

                    <li class="nav-item">
                        <a class="nav-link {{ Request::segment(2) === 'user' ? 'active' : null }}" href="{{ route('user.index') }}">
                            <i class="ni ni-circle-08 text-pink"></i>  {{ __('User Management') }}
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link {{ Request::segment(3) === 'retailers' ? 'active' : null }}" href="{{ route('user.retailers') }}">
                            <i class="ni ni-circle-08 text-purple"></i>  {{ __('Retailers') }}
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link {{ Request::segment(2) === 'email-settings' ? 'active' : null }}" href="{{ route('settings.email') }}">
                            <i class="ni ni-email-83 text-blue"></i>  {{ __('Email Templates') }}
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link {{ Request::segment(2) === 'settings' ? 'active' : null }}" href="{{ route('settings') }}">
                            <i class="ni ni-settings text-red"></i>  {{ __('Settings') }}
                        </a>
                    </li>

                @endrole

                @role('User')

                    <li class="nav-item">
                        <a class="nav-link {{ Request::segment(2) === 'purchase' ? 'active' : null }}" href="{{ route('purchase') }}">
                            <i class="ni ni-cart text-green"></i>  {{ __('Purchase History') }}
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link {{ Request::segment(2) === 'user-settings' ? 'active' : null }}" href="{{ route('user.settings') }}">
                            <i class="ni ni-settings text-red"></i>  {{ __('API Portal Settings') }}
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link {{ Request::segment(2) === 'pdf' ? 'active' : null }}" href="{{ route('pdf.agreement') }}">
                            <i class="ni ni-cloud-download-95 text-blue"></i>  {{ __('Download Agreement') }}
                        </a>
                    </li>

                @endrole

                <li class="nav-item mb-no nib-four" style="position: absolute; bottom: 0;">
                    <a title="{{ __('User profile') }}" style="" class="button-nav-1 nav-bottom-buttons pull-left logout-btn" href="{{ route('profile.edit') }}">
                        <i class="ni ni-circle-08"></i>
                    </a>
                    <a style="" class="pull-left nav-bottom-buttons text-red" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="ni ni-button-power"></i>
                    </a>
                </li>

            </ul>
            <!-- Divider -->

        </div>
    </div>
</nav>
