<?php use App\Helper\PA; ?>
@extends('layouts.app', ['title' => __('User Settings')])

@section('content')
    {{--@include('settings.partials.header', ['title' => __('Settings')])--}}
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">

                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('API Portal Settings') }}</h3>
                            </div>
                            <div class="col-4 text-right d-none">
                                <a href="{{ route('settings') }}"
                                   class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>

                    @include('layouts.status.success')

                    <div class="card-body">
                        <form method="post"
                              action="{{ (is_null($settings)) ? route('user.settings.create') : route('user.settings.update') }}"
                              autocomplete="off">
                            @csrf

                            <h6 class="heading-small text-muted mb-4">{{ __('API Portal Settings') }}</h6>

                            <div class="pl-lg-4">

                                <div class="row">


                                    <div class="col-md-6 d-none">

                                        <div class="form-group{{ $errors->has('url') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-name">{{ __('Url') }}</label>
                                            <input type="text" name="url" id="input-name"
                                                   class="form-control form-control-alternative{{ $errors->has('url') ? ' is-invalid' : '' }}"
                                                   placeholder="{{ __('Login Url') }}"
                                                   value="{{ (!is_null($settings)) ? $settings->url : '' }}" autofocus>

                                            @if ($errors->has('url'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('url') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="col-md-6">

                                        <div class="form-group{{ $errors->has('username') ? ' has-danger' : '' }}">
                                            <label class="form-control-label"
                                                   for="input-email">{{ __('API User Name') }}</label>
                                            <input type="text" name="username" id="input-email"
                                                   class="form-control form-control-alternative{{ $errors->has('username') ? ' is-invalid' : '' }}"
                                                   placeholder="{{ __('API User Name') }}"
                                                   value="{{ (!is_null($settings)) ? $settings->username : '' }}"
                                                   required>

                                            @if ($errors->has('username'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('username') }}</strong>
                                            </span>
                                            @endif
                                        </div>

                                    </div>

                                    <div class="col-md-6">

                                        <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                            <label class="form-control-label"
                                                   for="input-name">{{ __('API Password') }}</label>
                                            <div class="input-group" id="show_hide_password">
                                            <input style="{{ is_null($settings) ? 'width:88%;': ''  }}"
                                                   type="password" name="password" id="input-name"
                                                   class="form-control form-control-alternative{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                   placeholder="{{ __('API Password') }}"
                                                   value="{{ (!is_null($settings)) ? $settings->password : '' }}"
                                                   required autofocus>
                                                @if(is_null($settings))
                                                <div class="form-control form-control-alternative text-right">
                                                    <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                                </div>
                                                @endif
                                            </div>

                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('eventId') ? ' has-danger' : '' }}">

                                            <label class="form-control-label"
                                                   for="input-time">{{ __('Cart Submission Time') }}</label>

                                            <input type="text" name="time" id="input-time"
                                                   class="form-control timepicker form-control-alternative{{ $errors->has('time') ? ' is-invalid' : '' }}"
                                                   placeholder="{{ __('Time') }}"
                                                   value="{{ (!empty($settings) && !is_null($settings->time)) ? \App\Helper\PA::dateFormat('2019-05-09 ' . $settings->time, 'H:i') : '' }}"
                                                   required>

                                        </div>

                                    </div>

                                    <div class="col-md-6">

                                        <div class="form-group focused">
                                            <label class="form-control-label" for="timezone">Cart Submission Time
                                                Zone</label>
                                            <select name="timezone" class="form-control form-control-alternative">
                                                @foreach($timezone as $k => $zoneName )
                                                    <option value="{{ $zoneName }}" {{ (!is_null($settings) && $settings->timezone == $zoneName) ? 'selected' : '' }}>
                                                        {{ PA::cleanTimeZoneName($zoneName) }}
                                                    </option>
                                                @endforeach
                                            </select>


                                        </div>

                                    </div>

                                    <div class="col-md-6">

                                        <div class="form-group focused">
                                            <label class="form-control-label" for="input-name">
                                                {{ env('APP_NAME', '') }}
                                                Status
                                            </label>
                                            <select name="status" class="form-control form-control-alternative">
                                                <option value="1" {{ (!is_null($settings) && $settings->status == 1) ? 'selected' : '' }}>
                                                    Active
                                                </option>
                                                <option value="0" {{ (!is_null($settings) && $settings->status == 0) ? 'selected' : '' }}>
                                                    Inactive
                                                </option>
                                            </select>
                                        </div>

                                    </div>

                                    <div class="col-md-6">

                                        <div class="form-group focused">
                                            <label class="form-control-label" for="input-name">
                                                Cart Submission Days
                                            </label>

                                            <div class="row">
                                                <div class="col-md-3 col-6">
                                                    <div class="custom-control custom-checkbox mb-3">
                                                        <input name="days[]" class="custom-control-input" value="MON"
                                                               id="customCheck1"
                                                               type="checkbox" {{ in_array('MON', $weekDays) ? 'checked': '' }}>
                                                        <label class="custom-control-label"
                                                               for="customCheck1">Monday</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-6">
                                                    <div class="custom-control custom-checkbox mb-3">
                                                        <input name="days[]" class="custom-control-input" value="TUE"
                                                               id="customCheck2"
                                                               type="checkbox" {{ in_array('TUE', $weekDays) ? 'checked': '' }}>
                                                        <label class="custom-control-label"
                                                               for="customCheck2">Tuesday</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-6">
                                                    <div class="custom-control custom-checkbox mb-3">
                                                        <input name="days[]" class="custom-control-input" value="WED"
                                                               id="customCheck3"
                                                               type="checkbox" {{ in_array('WED', $weekDays) ? 'checked': '' }}>
                                                        <label class="custom-control-label"
                                                               for="customCheck3">Wednesday</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-6">
                                                    <div class="custom-control custom-checkbox mb-3">
                                                        <input name="days[]" class="custom-control-input" value="THU"
                                                               id="customCheck4"
                                                               type="checkbox" {{ in_array('THU', $weekDays) ? 'checked': '' }}>
                                                        <label class="custom-control-label"
                                                               for="customCheck4">Thursday</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 col-6">
                                                    <div class="custom-control custom-checkbox mb-3">
                                                        <input name="days[]" class="custom-control-input" value="FRI"
                                                               id="customCheck5"
                                                               type="checkbox" {{ in_array('FRI', $weekDays) ? 'checked': '' }}>
                                                        <label class="custom-control-label"
                                                               for="customCheck5">Friday</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-6">
                                                    <div class="custom-control custom-checkbox mb-3">
                                                        <input name="days[]" class="custom-control-input" value="SAT"
                                                               id="customCheck6"
                                                               type="checkbox" {{ in_array('SAT', $weekDays) ? 'checked': '' }}>
                                                        <label class="custom-control-label"
                                                               for="customCheck6">Saturday</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-6">
                                                    <div class="custom-control custom-checkbox mb-3">
                                                        <input name="days[]" class="custom-control-input" value="SUN"
                                                               id="customCheck7"
                                                               type="checkbox" {{ in_array('SUN', $weekDays) ? 'checked': '' }}>
                                                        <label class="custom-control-label"
                                                               for="customCheck7">Sunday</label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group focused">

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="custom-control custom-checkbox mb-3">
                                                        <input name="agreement" class="custom-control-input" value="agreed"
                                                               id="customCheck13"
                                                               type="checkbox" {{ (!is_null($settings)) ? 'checked' : '' }} required>
                                                        <label class=" custom-control-label"
                                                               for="customCheck13">
                                                            By filling the detail, you agree to our
                                                            <a class="mt-2" href="{{ route('pdf.agreement') }}">SAAS Agreement</a>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">

                                        <input type="hidden" class="exceptionDatesJS" name="exceptionDatesJS"
                                               value="{{ implode(',', $oldDateSelected)  }}">

                                        <label class="form-control-label" for="input-name">Exception Time</label>

                                        <div class="custom-control custom-checkbox mb-3">
                                            <input name="exceptionTime" class="custom-control-input"
                                                   id="datetimeCheckbox"
                                                   type="checkbox" {{ !empty($oldDateSelected) ? 'checked': '' }}>
                                            <label class="custom-control-label" for="datetimeCheckbox">Select</label>
                                        </div>

                                        <div class="row calenderException">
                                            <div class="col-md-6">
                                                <div id="datepicker" class=""></div>
                                                <input type="hidden" value="17-03-2020" id="alternate"
                                                       class="form-control form-control-alternative">
                                            </div>

                                            <div class="col-md-6">
                                                <div class="selected-dates">

                                                    @if(!empty($oldTimeSelected))

                                                        @foreach($oldTimeSelected as $k => $t)

                                                            <div class="timestamp ts-{{ $oldDateSelected[$k] }}">
                                                                <label class="form-control-label mt-2"
                                                                       for="input-name">{{ $oldDateSelected[$k] }}</label>
                                                                <input type="hidden" class="exDateField"
                                                                       value="{{ $oldDateSelected[$k] }}"
                                                                       name="exdate[]">
                                                                <input type="text" value="{{ $t }}"
                                                                       placeholder="Set Time" name="extime[]"
                                                                       id="input-time"
                                                                       class="form-control timepicker form-control-alternative"
                                                                       required>
                                                            </div>

                                                        @endforeach

                                                    @endif


                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="text-center">
                                    <button type="submit"
                                            class="btn btn-success mt-4">{{ __((is_null($settings)) ? 'Create' : 'Update') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="cloneDiv d-none">

            <div class="timestamp">
                <label class="form-control-label mt-2" for="input-name"></label>
                <input type="hidden" class="exDateField" name="exdate[]">
                <input type="text" placeholder="Set Time" name="extime[]" id="input-time"
                       class="form-control timepicker form-control-alternative" required>
            </div>

        </div>

        @include('layouts.footers.auth')
    </div>
@endsection
