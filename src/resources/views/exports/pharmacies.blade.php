
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Pharmacy Name</th>
        <th>Pharmacy Address</th>
        <th>Pharmacy Phone</th>
        <th>PBS Approval Number</th>
        <th>Role</th>
        <th>Execution Time</th>
        <th>Time Zone</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($users as $user)
        <tr>
            <td >{{ $user->name }}</td>
            <td >{{ $user->email }}</td>
            <td >{{ $user->pharmacy_name }}</td>
            <td >{{ $user->pharmacy_address }}</td>
            <td >{{ $user->pharmacy_phone }}</td>
            <td >{{ $user->pbs_number }}</td>
            <td >{{ $user->role }}</td>
            <td >{{ \App\Helper\PA::getUserSettings($user->id, 'time') }}</td>
            <td >{{ \App\Helper\PA::getUserSettings($user->id, 'timezone') }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>