@extends('layouts.app', ['title' => __('Email Templates')])

@section('content')

    @include('layouts.headers.cards')
    <div class="container-fluid mt--7">
        <div class="row">

            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="col-12 mb-0">{{ __('Email Templates') }}</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('settings.update') }}" autocomplete="off">
                            @csrf

                            <h6 class="heading-small text-muted mb-4">{{ __('Email Templates') }}</h6>
                            <input type="hidden" name="option_name" value="email_templates">
                            @if (session('status'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('status') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            <div class="row">

                                <div class="col-md-12">

                                    <div class="form-group{{ $errors->has('trialReminder') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-name">{{ __('Trial Reminder') }}</label>
                                        <textarea class="form-control" name="trialReminder" id="exampleFormControlTextarea2" rows="3" resize="none">{{ isset($et['trialReminder']) ? $et['trialReminder'] : ''  }}</textarea>

                                        @if ($errors->has('trialReminder'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('trialReminder') }}</strong>
                                        </span>
                                        @endif
                                    </div>

                                </div>

                                <div class="col-md-12">

                                    <div class="form-group{{ $errors->has('trialExpire') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-name">{{ __('Trial Expire') }}</label>
                                        <textarea class="form-control" name="trialExpire" id="input-name" rows="3" resize="none">{{ isset($et['trialExpire']) ? $et['trialExpire'] : ''  }}</textarea>


                                        @if ($errors->has('trialExpire'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('trialExpire') }}</strong>
                                        </span>
                                        @endif
                                    </div>

                                </div>



                            </div>
                            <div class="pl-lg-">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection