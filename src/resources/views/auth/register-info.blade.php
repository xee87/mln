@extends('layouts.app', ['class' => 'bg-default'])

@section('content')
    @include('layouts.headers.guest')

    <div class="container mt--8 pb-5">
        <!-- Table -->
        <div class="row justify-content-center">
            <div class="col-lg-10 col-md-10">
                <div class="card bg-secondary shadow border-0">

                    <div class="card-body px-lg-5 py-lg-5">
                        <div class="text-center text-muted mb-4">
                            <small>{{ __('User Information') }}</small>
                        </div>
                        <form id="{{ $user->hasVerifiedPhone() ? '' : 'verifyPhoneForm' }}" data-url="{{ route('verifyphone.send') }}"
                              role="form" method="POST" action="{{ route('register.info.store') }}">
                            @csrf
                            <input type="hidden" class="user_idF" name="user_id" value="{{ $user->id }}">
                            <input type="hidden" name="status" value="1">
                            <div class="row">

                                <div class="col-md-6">

                                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-email">{{ __('Pharmacist Name') }}</label>
                                        <div class="input-group input-group-alternative mb-3">

                                            <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Pharmacist Name') }}" type="text" name="name" value="{{ old('name') }}" required autofocus>
                                        </div>
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <div class="form-group{{ $errors->has('pharmacy_name') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-email">{{ __('Pharmacy Name') }}</label>
                                        <div class="input-group input-group-alternative mb-3">
                                            <input class="form-control{{ $errors->has('pharmacy_name') ? ' is-invalid' : '' }}" placeholder="{{ __('Pharmacy Name') }}" type="text" name="pharmacy_name" value="{{ old('pharmacy_name') }}" required autofocus>
                                        </div>
                                        @if ($errors->has('pharmacy_name'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('pharmacy_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <div class="form-group{{ $errors->has('pharmacy_email') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-email">{{ __('Pharmacy Email') }}</label>
                                        <div class="input-group input-group-alternative mb-3">
                                            <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Pharmacy Email') }}" type="email" name="pharmacy_email" value="{{ $user->email }}" readonly autofocus>
                                        </div>
                                        @if ($errors->has('pharmacy_email'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('pharmacy_email') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <div class="form-group{{ $errors->has('pharmacy_phone') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-email">{{ __('Pharmacy Phone') }}</label>
{{--                                        <span data-user="{{ $user->id }}" data-url="" data-toggle="modal" data-target="#verifyNumber" class="verify-number"><a href="javascript:void(0)">Verify</a></span>--}}
                                        <div class="input-group input-group-alternative mb-3">
                                            <input class="form-control phoneField {{ $errors->has('pharmacy_phone') ? ' is-invalid' : '' }}" placeholder="{{ __('Pharmacy Phone') }}"
                                               min="12"  data-inputmask="'mask': '+61 9 99 99 99 99'" type="text" name="pharmacy_phone" value="{{ old('pharmacy_phone') }}" required autofocus>
                                        </div>
{{--                                        @if ($errors->has('pharmacy_phone'))--}}
                                            <span class="invalid-feedback phone-error d-none " style="display: block;" role="alert">
                                                <strong>Invalid Number</strong>
                                            </span>
{{--                                        @endif--}}
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('address') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-email">{{ __('Address') }}</label>
                                        <div class="input-group input-group-alternative mb-3">
                                            <input id="location" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" placeholder="{{ __('Address') }}" type="text" name="address" value="{{ old('address') }}" required autofocus>
                                        </div>
                                        @if ($errors->has('address'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">

                                    <div class="form-group{{ $errors->has('role') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-email">{{ __('Role') }}</label>
                                        <div class="input-group input-group-alternative mb-3">
                                            <input class="form-control{{ $errors->has('role') ? ' is-invalid' : '' }}" placeholder="{{ __('Role') }}" type="text" name="role" value="{{ old('role') }}"
                                                   required autofocus>
                                        </div>
                                        @if ($errors->has('role'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label class="form-control-label" for="input-email">{{ __('PBS Status') }}</label><br>

                                        <input type="radio" id="yes-pbs" name="pbs" checked value="1">
                                        <label style="margin-right: 10px;" for="male">PBS APPROVED</label>

                                        <input type="radio" id="no-pbs" name="pbs" value="0">
                                        <label for="female">NOT PBS APPROVED</label><br>
                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <div id="pbs-number" class="form-group{{ $errors->has('pbs_number') ? ' has-danger' : '' }}">
                                        <label class="form-control-label" for="input-email">{{ __('PBS Approval number') }}</label>
                                        <div class="input-group input-group-alternative mb-3">
                                            <input class="form-control{{ $errors->has('role') ? ' is-invalid' : '' }}" placeholder="{{ __('PBS Approval number') }}" type="text" name="pbs_number" value="{{ old('pbs_number') }}"  autofocus>
                                        </div>
                                        @if ($errors->has('pbs_number'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                                 <strong>{{ $errors->first('pbs_number') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                            </div>


                            <div class="text-center">
                                <button type="submit" class="btn btn-primary mt-4">{{ __('Create account') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('modals.normal')
    @include('modals.verifyNumber')
@endsection


