@extends('layouts.app', ['title' => __('Purchase Items')])

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Purchase History') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('purchase') }}" class="btn btn-sm btn-primary">{{ __('Back') }}</a>
                            </div>
                        </div>
                    </div>


                    @include('layouts.status.success')

                    {{--<div class="table-responsive">--}}
                        <table class="table fixed-layout-table{{--align-items-center table-flush--}}">
                            <thead class="thead-light">
                            <tr>
                                <th >{{ __('Sr. #') }}</th>
                                <th >{{ __('PDE') }}</th>
                                <th >{{ __('Name') }}</th>
                                <th >{{ __('List Price') }}</th>
                                <th >{{ __('Discount') }}</th>
                                <th >{{ __('Your Price') }}</th>
                                <th >{{ __('SOH') }}</th>
                                <th >{{ __('Quantity') }}</th>
                                <th >{{ __('Subtotal ex. GST') }}</th>
                                <th >{{ __('Subtotal inc. GST') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($items as $item)
                                <tr>
                                    <td>{{ $item->sr_no }}</td>
                                    <td>{{ $item->pde }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->list_price }}</td>
                                    <td>{{ $item->discount }}</td>
                                    <td>{{ $item->your_price }}</td>
                                    <td>{{ $item->soh }}</td>
                                    <td>{{ $item->quantity }}</td>
                                    <td>{{ $item->subtotal_ex }}</td>
                                    <td>{{ $item->subtotal_inc }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    {{--</div>--}}
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            {{ $items->links() }}
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>

@endsection