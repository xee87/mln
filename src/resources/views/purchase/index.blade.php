@extends('layouts.app', ['title' => __('Purchases')])

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Purchases') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                {{--<a href="{{ route('user.create') }}" class="btn btn-sm btn-primary">{{ __('Add user') }}</a>--}}
                            </div>
                        </div>
                    </div>


                    @include('layouts.status.success')

                    {{--<div class="table-responsive">--}}
                        <table class="table fixed-layout-table {{--align-items-center table-flush--}}">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">{{ __('Time') }}</th>
                                <th scope="col">{{ __('Message') }}</th>
                                <th scope="col">{{ __('Status') }}</th>
                                <th scope="col">{{ __('Total Items') }}</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($purchases as $p)
                                <tr>
                                    {{--<td>{{ date('d/m/Y H:i', $p->timestamp) }}</td>--}}
                                    <td>{{ \App\Helper\PA::getTimeZoneBasedDate($p->timestamp) }}</td>
                                    <td>
                                        {{ ($p->message) ? $p->message : 'none'  }}
                                    </td>

                                    <td>{{ \App\Helper\PA::getStatus($p->status) }}</td>
                                    <td>{{ \App\Helper\PA::totalPurchaseItems($p->id) }}</td>
                                    <td class="text-right">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                <a class="dropdown-item" href="{{ route('purchase.items', ['id' => $p->id]) }}">{{ __('Items') }}</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    {{--</div>--}}
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            {{ $purchases->links() }}
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection