<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<style>
    h1{
        font-size: 18px !important;
    }

    h2{
        font-size: 16px !important;
    }
</style>
<body>

<div style="text-align: center;">
    <img style="max-height: 6.5rem;" src="{{ asset('argon/img/brand/pa.png') }}"  alt="...">
</div>

<div style="text-align: center;">
    <h1>SAAS SERVICES AGREEMENT</h1>
</div>

<p>This SaaS Services Agreement (&ldquo;Agreement&rdquo;) is entered into on <b>{{date('M d, Y',strtotime($user->created_at))}}</b> between</p>
<p>{{ env('APP_NAME') }} ABN 67358896297 (&ldquo;<strong>Business&rdquo;</strong>),</p>
<p>and</p>
<p>{{$user->name}} (<strong>&ldquo;Customer&rdquo;</strong>).</p>
<p><br /> </p>
<ol>
    <li>&nbsp;&nbsp;&nbsp;This Agreement includes and incorporates Schedules 1 and 2, as well as the attached Terms and Conditions and contains, among other things, warranty disclaimers, liability limitations and use limitations.</li>
    <li>&nbsp;&nbsp;&nbsp;Except as otherwise permitted by this Agreement, no variation to its terms will be effective unless in writing and signed by both the Business and the Customer.</li>
</ol>
<p><br /> </p>
<h2>1.&nbsp;&nbsp;&nbsp;&nbsp; SaaS Services and Support</h2>
<ul>
    <li>Subject to the terms of this Agreement, the Business will use commercially reasonable efforts to provide the Customer the Services in accordance with the Service Level Terms attached in Schedule 1.</li>
    <li>Subject to this Agreement, the Business will provide the Customer with reasonable technical support services in accordance with the Business&rsquo;s standard practice.</li>
</ul>
<h2>2.&nbsp;&nbsp;&nbsp;&nbsp; Restrictions and Responsibilities</h2>
<ul>
    <li>Except as otherwise permitted by the Copyright Act 1968 (Cth) or agreed to in writing by {{ env('APP_NAME') }}, the Customer must not reverse compile, disassemble, remove, release, disclose, reveal, copy, extract, modify or otherwise reverse engineer all or any part of the Services or any software, documentation or data related to the Services</li>
    <li>The Customer represents, covenants, and warrants that the Customer will use the Services only in compliance with the Business&rsquo;s standard published policies then in effect (the &ldquo;Policy&rdquo;) and all applicable laws and regulations.</li>
    <li>The Customer hereby agrees to indemnify and hold harmless the Business against any damages, losses, liabilities, settlements and expenses (including without limitation costs and legal fees) in connection with any claim or action that arises from an alleged violation of the foregoing or otherwise from the Customer&rsquo;s use of Services. Although the Business has no obligation to monitor the Customer&rsquo;s use of the Services, the Business may do so and may prohibit any use of the Services it believes may be (or alleged to be) in violation of the foregoing.</li>
    <li>The Customer shall be responsible for obtaining and maintaining any equipment and ancillary services needed to connect to, access or otherwise use the Services, including, without limitation, modems, hardware, servers, software, operating systems, networking, web servers and the like (collectively, &ldquo;Equipment&rdquo;). The Customer shall also be responsible for maintaining the security of the Equipment, the Customer account, passwords (including but not limited to administrative and user passwords) and files, and for all uses of the Customer account or the Equipment with or without the Customer&rsquo;s knowledge or consent.</li>
</ul>
<h2>3.&nbsp;&nbsp;&nbsp;&nbsp;Confidentiality and Proprietary Rights</h2>
<ul>
    <li>Each party (the &ldquo;Receiving Party&rdquo;) understands that the other party (the &ldquo;Disclosing Party&rdquo;) has disclosed or may disclose business, technical or financial information relating to the Disclosing Party&rsquo;s business (referred to as &ldquo;Proprietary Information&rdquo; of the Disclosing Party).</li>
    <li>Proprietary Information of the Business includes non-public information regarding features, functionality and performance of the Service. Proprietary Information of the Customer includes non-public data provided by the Customer to the Business to enable the provision of the Services (&ldquo;Customer Data&rdquo;).
        <ol>
            <li>The Receiving Party agrees:
                <ol>
                    <li>to take reasonable precautions to protect such Proprietary Information, and</li>
                    <li>not to use (except in performance of the Services or as otherwise permitted in this Agreement) or divulge to any third person any such Proprietary Information. The Disclosing Party agrees that the foregoing shall not apply with respect to any information after five (5) years following the disclosure thereof or any information that the Receiving Party can document (a)&nbsp;is or becomes generally available to the public, or (b)&nbsp;was in its possession or known by it prior to receipt from the Disclosing Party, or (c)&nbsp;was rightfully disclosed to it without restriction by a third party, or (d)&nbsp;was independently developed without use of any Proprietary Information of the Disclosing Party or (e)&nbsp;is required to be disclosed by law.</li>
                </ol>
            </li>
        </ol>
    </li>
</ul>
<ul>
    <li>The Customer shall own all right, title and interest in and to the Customer Data, as well as any data that is based on or derived from the Customer Data and provided to the Customer as part of the Services. The Business shall own and retain all right, title and interest in and to (a) the Services, all improvements, enhancements or modifications thereto, (b) any software, applications, inventions or other technology developed in connection with Implementation Services or support, and (c) all intellectual property rights related to any of the foregoing.</li>
    <li>Notwithstanding anything to the contrary, the Business shall have the right collect and analyse data and other information relating to the provision, use and performance of various aspects of the Services and related systems and technologies (including, without limitation, information concerning Customer Data and data derived therefrom), and the Business will be free (during and after the term) to (i) use such information and data to improve and enhance the Services and for other development, diagnostic and corrective purposes in connection with the Services and other Business offerings, and (ii) disclose such data solely in aggregate or other de-identified form in connection with its business.</li>
    <li>No rights or licenses are granted except as expressly set out in this Agreement.</li>
</ul>
<h2>4.&nbsp;&nbsp;&nbsp;&nbsp; Payment of Fees</h2>
<ul>
    <li>The Customer will pay the Business the then applicable fees described in the Order Form for the Services and Implementation Services in accordance with this Agreement (the &ldquo;Fees&rdquo;).</li>
    <li>If the Customer&rsquo;s use of the Services exceeds the Service Capacity set forth on the Order Form or otherwise requires the payment of additional fees (per the terms of this Agreement), the Customer shall be billed for such usage and the Customer agrees to pay the additional fees in the manner provided in this Agreement.</li>
    <li>The Business reserves the right to change the Fees or applicable charges and to institute new charges and Fees at the end of the Initial Service Term or thencurrent renewal term, upon thirty&nbsp;(30) days prior notice to the Customer (which may be sent by email).</li>
    <li>If the Customer believes that the Business has billed the Customer incorrectly, The Customer must contact the Business no later than 60&nbsp;days after the closing date on the first billing statement in which the error or problem appeared, in order to receive an adjustment or credit. Inquiries should be directed to the Business&rsquo;s customer support department.</li>
    <li>The Business may choose to bill through an invoice, in which case, full payment for invoices issued in any given month must be received by the Business thirty&nbsp;(30) days after the mailing date of the invoice.</li>
    <li>Unpaid amounts are subject to an interest charge of 1.5% per month on any outstanding balance, or the maximum permitted by law, whichever is lower, plus all expenses of collection and may result in immediate termination of Service.</li>
    <li>Where applicable, any goods or services tax, charge, impost or duty payable in respect of this Agreement or the supply of any goods or service made under or in respect of this Agreement and any other taxes, duties or levies will be paid by the Customer at the then prevailing rate.</li>
</ul>
<h2>5.&nbsp;&nbsp;&nbsp;&nbsp; Term and Termination</h2>
<ul>
    <li>Subject to earlier termination as provided below, this Agreement is for the Initial Service Term as specified in the Order Form, and shall be automatically renewed for additional periods of the same duration as the Initial Service Term (collectively, the &ldquo;Term&rdquo;), unless either party requests termination at least thirty (30) days prior to the end of the then-current term.</li>
    <li>In addition to any other remedies it may have, either party may also terminate this Agreement upon thirty (30) days&rsquo; notice (or without notice in the case of non-payment), if the other party materially breaches any of the terms or conditions of this Agreement. The Customer will pay in full for the Services up to and including the last day on which the Services are provided. Upon any termination, the Business will make Customer Data available to the Customer in a form the Business deems appropriate for a period of thirty (30) days, but thereafter the Business may, but is not obligated to, delete stored Customer Data. All sections of this Agreement which by their nature should survive termination will survive termination, including, without limitation, accrued rights to payment, confidentiality obligations, warranty disclaimers, and limitations of liability.</li>
</ul>
<h2>6.&nbsp;&nbsp;&nbsp;&nbsp; Warranty and Disclaimer</h2>
<ul>
    <li>The Business shall use reasonable efforts consistent with prevailing industry standards to maintain the Services in a manner which minimizes errors and interruptions in the Services and shall perform the Implementation Services in a professional and proper manner. Services may be temporarily unavailable for scheduled maintenance or for unscheduled emergency maintenance, either by the Business or by third-party providers, or because of other causes beyond the Business&rsquo;s reasonable control, but the Business shall use reasonable efforts to provide advance notice in writing or by e-mail of any scheduled service disruption. However, the Business does not warrant that the Services will be uninterrupted or error free; nor does it make any warranty as to the results that may be obtained from use of the Services.</li>
    <li>While all due care has been taken, the Business does not warrant that the operation of the Services will be uninterrupted or error free or that any third party components of the Services, will be accurate or error free or that the Services will be compatible with any application, program or software not specifically identified as compatible by the Business.</li>
    <li>The Business&rsquo;s obligation and the Customer&rsquo;s exclusive remedy during the Licence Period and any Renewal Period are limited, in the Business&rsquo;s absolute discretion, to:
        <ol>
            <li>The Business, at its own expense, using all reasonable endeavours to rectify any non-conformance of the Services by repair (by way of a patch, work around, correction or otherwise) within a reasonable period of time; or</li>
            <li>a refund of the Fees paid if, in the Business&rsquo;s reasonable opinion, it is unable to rectify such non-conformance within a reasonable timescale or at an economic cost, whereupon this Agreement will terminate.</li>
        </ol>
    </li>
    <li>The Customer acknowledges and accepts that it is the Customer&rsquo;s sole responsibility to ensure that:
        <ol>
            <li>the facilities and functions of the Services meet the Customer&rsquo;s requirements;</li>
            <li>the Services are appropriate for the specific circumstance of the Customer and are within the laws and regulations of the Customer&rsquo;s jurisdiction.</li>
            <li>the Business does not purport to provide any legal, taxation or accountancy advice by providing the Service under this Agreement.</li>
        </ol>
    </li>
    <li>The Business will not be liable for any failure of the Services to provide any function not described in the documentation (provided online as part of the Services) or any failure attributable to:
        <ol>
            <li>any modification to the Services other than by the Business;</li>
            <li>accident, abuse or misapplication of Services by the Customer;</li>
            <li>use of the Services with other software or equipment without the Business&rsquo;s written consent;</li>
            <li>use of other than the latest, unaltered current release of the Services;</li>
            <li>or use other than in accordance with this Agreement.</li>
        </ol>
    </li>
    <li>If, upon investigation, a problem with the Services is determined not to be the Business&rsquo;s responsibility, the Business may invoice the Customer immediately for all reasonable costs and expenses incurred by the Business in the course of or in consequence of such investigation.</li>
</ul>
<h2>7.&nbsp;&nbsp;&nbsp;&nbsp; Indemnity</h2>
<ul>
    <li>The Customer must indemnify and hold the Business harmless from and against all claims and losses arising from loss, damage, liability, injury to the Business, its employees and third parties, infringement of third party intellectual property, or third party losses by reason of or arising out of any information supplied to the Customer by the Business, its employees or suppliers, or supplied to the Business by the Customer within or without the scope of this Agreement.</li>
</ul>
<h2>8.&nbsp;&nbsp;&nbsp;&nbsp; Limitation on liability</h2>
<ul>
    <li>Except in the case of death or personal injury caused by the Business's negligence, the liability of the Business under or in connection with this agreement whether arising in contract, tort, negligence, breach of statutory duty or otherwise must not exceed the fees paid by the Customer to the Business for the services under this agreement in the 12 months prior to the act that gave rise to the liability, in each case, whether or not the Business has been advised of the possibility of such damages.</li>
    <li>Neither party is liable to the other party in contract, tort, negligence, breach of statutory duty or otherwise for any loss, damage, costs or expenses of any nature whatsoever incurred or suffered by that other party of an indirect or consequential nature including any economic loss or other loss of turnover, profits, business or goodwill.</li>
</ul>
<h2>9.&nbsp;&nbsp;&nbsp;&nbsp; General</h2>
<ul>
    <li>If any provision of this Agreement is found to be unenforceable or invalid, that provision will be limited or eliminated to the minimum extent necessary so that this Agreement will otherwise remain in full force and effect and enforceable.</li>
    <li>This Agreement is not assignable, transferable or sublicensable by the Customer except with the Business&rsquo;s prior written consent. The Business may transfer and assign any of its rights and obligations under this Agreement without consent.</li>
    <li>This Agreement is the complete and exclusive statement of the mutual understanding of the parties and supersedes and cancels all previous written and oral agreements, communications and other understandings relating to the subject matter of this Agreement, and that all waivers and modifications must be in a writing signed by both parties, except as otherwise provided in this Agreement.</li>
    <li>No agency, partnership, joint venture, or employment is created as a result of this Agreement and the Customer does not have any authority of any kind to bind the Business in any respect whatsoever. In any action or proceeding to enforce rights under this Agreement, the prevailing party will be entitled to recover costs and legal fees.</li>
    <li>All notices under this Agreement will be in writing and will be deemed to have been duly given when received, if personally delivered; when receipt is electronically confirmed, if transmitted by facsimile or e-mail; the day after it is sent, if sent for next day delivery by recognised overnight delivery service; and upon receipt, if sent by certified or registered mail, return receipt requested.</li>
    <li>This Agreement takes effect, is governed by, and will be construed in accordance with the laws from time to time in force in New South Wales, Australia. The Parties submit to the non-exclusive jurisdiction of the courts of New South Wales.</li>
</ul>
<h4>Executed as a deed</h4>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <p><strong>Executed</strong> by {{ env('APP_NAME') }} ABN 67358896297 in accordance with section 127(1) of the <em>Corporations Act 2001 (Cth)</em>:</p>
        </td>
        <td>
            <p>&nbsp;</p>
        </td>
    </tr>
    <tr>
        <td>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>...................................................</p>
        </td>
        <td>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>...................................................</p>
        </td>
    </tr>
    <tr>
        <td>
            <p>Signature of director</p>
        </td>
        <td>
            <p>Signature of director or Business secretary</p>
        </td>
    </tr>
    <tr>
        <td>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>...................................................</p>
        </td>
        <td>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>...................................................</p>
        </td>
    </tr>
    <tr>
        <td>
            <p>Name (please print)</p>
        </td>
        <td>
            <p>Name (please print)</p>
        </td>
    </tr>
    <tr>
        <td>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </td>
        <td>
            <p>&nbsp;</p>
        </td>
    </tr>
    </tbody>
</table>
<p>&nbsp;</p>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <p><strong>Executed</strong> by ...... in accordance with section&nbsp;127 of the Corporations Act 2001 (Cth) by:</p>
        </td>
        <td>
            <p>&nbsp;</p>
        </td>
    </tr>
    <tr>
        <td>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>...................................................</p>
        </td>
        <td>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>...................................................</p>
        </td>
    </tr>
    <tr>
        <td>
            <p>Signature of Director</p>
        </td>
        <td>
            <p>Print of Director</p>
        </td>
    </tr>
    <tr>
        <td>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>...................................................</p>
        </td>
        <td>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>...................................................</p>
        </td>
    </tr>
    <tr>
        <td>
            <p>Signature of Director/Secretary</p>
        </td>
        <td>
            <p>Print name of Director/Secretary</p>
        </td>
    </tr>
    </tbody>
</table>
<h4>SCHEDULE 1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Specifications</h4>
<p><strong>Statement of Work</strong></p>

<p>1- Setting up an account to log into our portal for a 14 days trial.</p>
<p>&nbsp;2- After the trial period, if the store (Customer) decided to continue to use the service. The (Customer) will need to pay an agreed fee every month.</p>
<p>&nbsp;3- The total cost for the set-up will be $0.</p>
<p>&nbsp;4- If the (Customer) decided not to continue the service, no fees would be charged.</p>
<p>&nbsp;5- {{ env('APP_NAME') }} will send an email with the ordered items as pdf file as a reference to the (Customer).</p>
<p>&nbsp;6- {{ env('APP_NAME') }} will send you an email if the order transmission fails so that you can transmit the order manually.<br /> </p>
<p><br /> </p>
<p><strong>Service Level Terms</strong></p>
<p>1- Our service is to submit the order already entered into the wholesaler(s) portal (api) ONLY. It does not include adding/removing items to/from the shopping cart.&nbsp;</p>
<p>&nbsp;2- The service reliability will be 95% and will depend on the wholesaler(s) website. e.g., If the Wholesaler(s) website is not behaving normally, the transmission will fail.&nbsp;</p>
<p>&nbsp;3- Technical issues will be resolved within a maximum of 3 working days, as long as the Wholesaler(s) website is functioning as usual. During that time, order submission is the store (Customer) responsibility.&nbsp;</p>
<p>&nbsp;4- In the case of the Wholesaler(s) changing/upgrading their website, some code modifications will be required from our end, which might take up to 10 business days.&nbsp;</p>
<p>&nbsp;6- If the Wholesaler(s) added a new security measure, the service will be affected; Our support team will endeavor to resolve the issue, and they will communicate the result.</p>
<p>In some cases, that might take up to 10 business days. During this time, transmitting the order will be the (Customer) responsibility.&nbsp;</p>
<p>&nbsp;5- If the store (Customer) changes its username and/or password login to the Wholesaler(s), the service will not be performed. It is the responsibility of the store (Customer) to update this information on our portal.&nbsp;</p>
<p>&nbsp;6- If an item or medication has not come in the order, you will have to contact your Wholesaler(s) and find out why. {{ env('APP_NAME') }} take no responsibility for that. Our system is to transmit the order ONLY.&nbsp;</p>
<p>&nbsp;7- It is the (Customer) responsibility to set up the order transmission time before their Wholesaler(s) cut-off time for daily orders and according to the (Customer) timezone.</p>
<p>&nbsp;8- The Customer should account for a 10-15 minutes buffering period between the time they set-up on our portal and the Wholesaler(s) cut off time.&nbsp;</p>
<p>&nbsp;</p>
<p><strong>Support Terms</strong></p>
<p>The Business will provide Technical Support to the Customer via email. The Customer may initiate a helpdesk ticket emailing support@pharmaanywhere.com. The Business will use commercially reasonable efforts to respond to all Helpdesk tickets within three to five business days.</p>
<p><br />   </p>
<h4>SCHEDULE 2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SAAS Services Order Form</h4>
<p> </p>
<p><strong>Customer:</strong> {{$user->name}}</p>
<p><strong>Phone:</strong> {{$user->pharmacy_phone}}</p>
<p><strong>Address:</strong> {{$user->address}}</p>
<p><strong>Email:</strong> {{$user->email}}</p>
<p><strong>Services:</strong> Web managed wholesaler(s) (api) order submission. (the &ldquo;Service(s)&rdquo;)</p>
<p><strong>Services Fees:</strong> $30 AUD per month, payable in advance, subject to the terms of Section 4 of this Agreement.</p>
<p><strong>Initial Service Term:</strong> On month by month basis. Year (s)</p>
<p><strong>Service Capacity:</strong> One order transmission every day or on specific days at a predetermined time by the (Customer)</p>
<p><strong>Implementation Services:</strong> the Business will use commercially reasonable efforts to provide the Customer the services described in the Statement of Work (&ldquo;SOW&rdquo;) found in Schedule 1 of this Agreement (&ldquo;Implementation Services&rdquo;), and the Customer shall pay the Businessss the Implementation Fee in accordance with the terms of this Agreement.</p>
<p>&nbsp;</p>
<p><strong>Pilot use:</strong> if the Services use during the Pilot Period will be restricted to non-productive evaluation use. During pilot/evaluation use: (1) no fees will apply, except for any Pilot Use Fee specified below, (2) the Services are provided &ldquo;AS IS&rdquo; and no warranty obligations of the Business will apply, and (3) the Customer may terminate this Agreement and all of its rights hereunder by providing the Business written notice thereof no less than 10 days prior to the end of the Pilot Period; otherwise, this Agreement shall continue in effect for the Initial Service Term (subject to e<strong>arlier termination as provided in the Agreement).</strong></p>
<p>&nbsp;</p>
<p><strong>Pilot Period:</strong> 14 days</p>
<p><strong>Pilot Use Fee:</strong> $0</p>
<p>&nbsp;</p>
<pre class="gr gs gt gu gv hp hq hr"><span id="f1ab" class="hs ht ap ce hu b ei hy hz ia ib ic hw r hx" data-selectable-paragraph="">&nbsp;</span></pre>

</body>
</html>
