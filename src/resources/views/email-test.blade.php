


        <form style="padding: 200px" method="post" action="{{ route('email.get') }}">

        @csrf

            @if(session()->has('success'))
                <h2 style="color: green">{{ session()->get('success') }}</h2>
            @endif

            @if(session()->has('error'))
                <h2 style="color: green">{{ session()->get('error') }}</h2>
            @endif


            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label><br>
                <input style="padding: 10px; width: 100%" type="email" name="email" class="form-control"
                       id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" required><br><br>
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <br>
            <button style="padding: 10px; width: 100px" type="submit" class="btn btn-primary">Send</button>


        </form>

