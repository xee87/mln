@extends('layouts.app', ['title' => __('Settings')])

@section('content')
    {{--@include('settings.partials.header', ['title' => __('Settings')])--}}
    @include('layouts.headers.cards')


    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Settings') }}</h3>
                            </div>
                            <div class="col-4 text-right d-none">
                                <a href="{{ route('settings') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>

                    @include('layouts.status.success')

                    <div class="card-body">

                            
                        <h6 class="heading-small text-muted mb-4">{{ __('Amazon AWS Settings ') }}<br>
                            <span style="color: red">Changing value in this setting may result in interuption of website. Please proceed.</span>
                        </h6>
                        <form method="post" action="{{ route('settings.update') }}" autocomplete="off">
                            @csrf
                            <div class="pl-lg-4">

                                <div class="row">

                                    <input type="hidden" name="option_name" value="aws_settings">


                                    <div class="col-md-6">

                                        <div class="form-group{{ $errors->has('awsKey') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-email">{{ __('AWS Key') }}</label>
                                            <input type="text" name="awsKey" id="input-email" class="form-control form-control-alternative{{ $errors->has('awsKey') ? ' is-invalid' : '' }}" placeholder="{{ __('AWS Key') }}" value="{{ isset($aws_setting['awsKey']) ? $aws_setting['awsKey'] : ''  }}" required>

                                            @if ($errors->has('awsKey'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('awsKey') }}</strong>
                                            </span>
                                            @endif
                                        </div>

                                    </div>

                                    <div class="col-md-6">

                                        <div class="form-group{{ $errors->has('awsSecret') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-name">{{ __('AWS Secret') }}</label>
                                            <input type="password" name="awsSecret" id="input-name" class="form-control form-control-alternative{{ $errors->has('awsSecret') ? ' is-invalid' : '' }}" placeholder="{{ __('AWS Secret') }}" value="{{ isset($aws_setting['awsSecret']) ? $aws_setting['awsSecret'] : ''  }}" required autofocus>

                                            @if ($errors->has('awsSecret'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('awsSecret') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-6">

                                        <div class="form-group{{ $errors->has('lambdaArn') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-name">{{ __('Lambda Arn') }}</label>
                                            <input type="text" name="lambdaArn" id="input-name" class="form-control form-control-alternative{{ $errors->has('lambdaArn') ? ' is-invalid' : '' }}" placeholder="{{ __('Lambda Arn') }}" value="{{ isset($aws_setting['lambdaArn']) ? $aws_setting['lambdaArn'] : ''  }}" required autofocus>

                                            @if ($errors->has('lambdaArn'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('lambdaArn') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('kmsKeyId') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-email">{{ __('KMS KeyId') }}</label>
                                            <input type="text" name="kmsKeyId" id="input-email" class="form-control form-control-alternative{{ $errors->has('kmsKeyId') ? ' is-invalid' : '' }}" placeholder="{{ __('KMS KeyId') }}" value="{{ isset($aws_setting['kmsKeyId']) ? $aws_setting['kmsKeyId'] : ''  }}" required >

                                            @if ($errors->has('kmsKeyId'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('kmsKeyId') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('dataRegion') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-email">{{ __('Data Region') }}</label>
                                            <input type="text" name="dataRegion" id="input-email" class="form-control form-control-alternative{{ $errors->has('dataRegion') ? ' is-invalid' : '' }}" placeholder="{{ __('Data Region') }}" value="{{ isset($aws_setting['dataRegion']) ? $aws_setting['dataRegion'] : ''  }}" required >

                                            @if ($errors->has('dataRegion'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('dataRegion') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>




                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>


                            </div>
                        </form>
                        <hr class="my-4" />

                        <h6 class="heading-small text-muted mb-4">{{ __('General Settings') }}</h6>
                        <form method="post" action="{{ route('settings.update') }}" autocomplete="off">
                            @csrf
                            <div class="pl-lg-4">
                                <input type="hidden" name="option_name" value="general_settings">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('trialDays') ? ' has-danger' : '' }}">
                                            <label class="form-control-label" for="input-email">{{ __('Trial Limit (Days)') }}</label>
                                            <input type="number" name="trialDays" id="input-email" class="form-control form-control-alternative{{ $errors->has('trialDays') ? ' is-invalid' : '' }}" placeholder="{{ __('Trial Limit') }}" value="{{ isset($gen_setting['trialDays']) ? $gen_setting['trialDays'] : '' }}" required >

                                            @if ($errors->has('trialDays'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('trialDays') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        @include('layouts.footers.auth')
    </div>
@endsection