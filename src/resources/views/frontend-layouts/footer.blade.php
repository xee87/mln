<!-----Footer Start here --->
<footer class="stem-footer">
    <footer class="footer_wrapper">
        <div class="row footer-part">
            <div class="large-12 columns">
                <div class="row">
                    <div class="large-3 columns">
                        <h4 class="footer-title">About Us</h4>

                        <div class="divdott"></div>
                        <img class="botlogo" src="{{asset('argon/img/brand/Order1G-Black.png')}}" alt=""/>

                        <div class="footer_part_content">
                            <p>Medico Theme Bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.
                                Duis sed odio sit amet nibh vulputate</p>
                        </div>
                    </div>
                    <div class="large-3 columns">
                        <h4 class="footer-title blue-title">Join Our Email Newsletter</h4>
                        <div class="footer_part_content">
                            <ul class="about-info">
                                <li><span> Receive Our catalogues By Email</span></li>
                                <li><span>Special Offers & Discount Direct in Your Inbox!</span></li>
                            </ul>
                        </div>
                        <div class="input-user by-email">
                            {{--<h4 class="blue-title">Simply enter your email bellow</h4>--}}
                            <form action="{{route('email.user')}}" method="post">
                                @csrf
                                <input type="email" name="email" class="email-user {{ $errors->has('email') ? 'is invalid' : ''}}" placeholder="Enter Your Email" value="">
                                <button type="submit" class="button bg-green-footer">Join Now</button>
                            </form>
                        </div>

                    </div>


                    <div class="large-3 columns">
                        <h4 class="footer-title">Contact info</h4>

                        <div class="divdott"></div>
                        <div class="footer_part_content">
                            <p>Medico Bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id </p>
                            <ul class="about-info">
                                <li><i class="icon-home"></i><span>lorem ipsum street</span></li>
                                <li><i class="icon-phone"></i><span>+399 (500) 321 9548</span></li>
                                <li><i class="icon-envelope"></i><a href="mailto:info@Medico.com">info@Medico.com</a>
                                </li>
                            </ul>
                            <ul class="social-icons">
                                <li><a href="#"><i class="icon-pinterest"></i></a></li>
                                <li><a href="#"><i class="icon-twitter"></i></a></li>
                                <li><a href="#"><i class="icon-facebook"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="large-3 columns">
                        <h4 class="footer-title">Quick Contact</h4>

                        <div class="divdott"></div>
                        <div class="small-12">
                            @if (session('status'))
                                <p class="success-message">{{ session('status') }}</p>
                            @endif
                        </div>
                        <form method="POST" action="{{ route('contact.save') }}" id="footer-contact-form">
                            @csrf
                            <div class="footer_part_content">
                                <div class="row">
                                    <div class="large-6 columns">
                                        <input type="text" placeholder="Name" name="name"/>
                                    </div>
                                    <div class="large-6 columns">
                                        <input style="height: 30px;" type="email" placeholder="Email" name="email"/>
                                    </div>
                                    <div class="large-12 columns">
                                        <textarea cols="10" rows="15" name="usermessage" placeholder="Message"></textarea>
                                    </div>
                                    <div class="large-12 columns text-right">
                                        <button type="submit" class="button">Send</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="privacy footer_bottom">
            <div class="footer-part">
                <div class="row">
                    <div class="large-10 columns copyright">
                        <p>&copy; <?php echo date('Y');?> Order Genie All Rights Reserved.</p>
                    </div>
                    <div class="large-2 columns">
                        <div id="back-to-top"><a href="#top"></a></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</footer>

<script type="text/javascript">
    $.ajaxSetup({
    headers:{

    }
    });

</script>
<!-----Footer End here --->




