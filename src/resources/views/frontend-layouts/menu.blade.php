<!-- Begin Main Wrapper -->
<div class="main-wrapper main-wrapper-position">
    <!-- Main Navigation -->
    <header class="row main-navigation">
        <div class="large-3 columns">
            <a href="{{ env('APP_URL') }}" id="logo">
                <img src="{{asset('argon/img/brand/ORIGINAL-SIZE.png')}}" alt="Medico Logo"/>
            </a>
        </div>
        <div class="large-9 columns">
            <nav class="top-bar">
                <ul class="title-area">
                    <li class="name"></li>
                    <li class="toggle-topbar menu-icon"><a href="#"><span>Main Menu</span></a></li>
                </ul>
                <section class="top-bar-section">
                    <!-- Left Nav Section -->
                    <ul class="right">
                        <li class="">
                            <a href="{{ route('home') }}" class="{{ Request::segment(1) === null ? 'active' : null }}">
                                Home
                            </a>
                        <li class="">
                            <a class="{{ Request::segment(1) === 'about-us' ? 'active' : null }}" href="{{route('about.us') }}">About Us</a>
                        </li>

                        <li class="">
                            <a class=" {{Request::segment(1)=== 'register' ? 'active' :null }}" href="{{ \Illuminate\Support\Facades\URL::to('register') }}" >SIGN UP</a>
                        </li>
                        <li class=""><a class="{{Request::segment(1)=== 'login' ? 'active' : null }}"href="{{ \Illuminate\Support\Facades\URL::to('login') }}" >LOGIN</a>
                        </li>
                        <li><a class="{{Request::segment(1)=== 'contact-us' ? 'active' : null }}" href="{{ route('contact.us') }}" >Contact</a></li>
                    </ul>
                    <!-- End Left Nav Section -->
                </section>
            </nav>
        </div>
    </header>
</div>
