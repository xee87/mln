<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width"/>
    <link href="{{ asset('argon/img/brand/favicon.png') }}" rel="icon" type="image/png">
    <title>
        @hasSection('page-title') @yield('page-title') {{ ' - '.env('APP_NAME', 'Pharmacy Automation') }} @else {{ env('APP_NAME', 'Pharmacy Automation') }} @endif
    </title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content='Never miss your order Cut-Off time again!'/>
    <meta property="og:description" content="Never miss your order Cut-Off time again!"/>

    <link href="{{asset('css/fonts/googleapifonts.css')}}" rel="stylesheet">

    <!-- Smallipop CSS - Tooltips -->

    <link href="{{asset('css/frontend/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/frontend/smallipop.css')}}" rel="stylesheet">

    <!-- Default CSS -->
    <link href="{{asset('css/frontend/normalize.css')}}" rel="stylesheet">
    <link href="{{asset('css/frontend/foundation.css')}}" rel="stylesheet">
    <link href="{{asset('css/frontend/fgx-foundation.css')}}" rel="stylesheet">

    <!-- Font Awesome - Retina Icons -->
    <link href="{{asset('css/fonts/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/frontend/nivo-slider.css')}}" rel="stylesheet">
    <link href="{{asset('css/frontend/default.css')}}" rel="stylesheet">
    <link href="{{asset('css/frontend/metalic.css')}}" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet">


    <!-- Google Fonts -->
    <link href="{{asset('css/fonts/fontsgoolge.css')}}" rel="stylesheet">
    <link href="{{asset('css/frontend/style.css')}}" rel="stylesheet">

{{--    <link href="{{asset('css/bootstrap/bootstrap.css')}}" rel="stylesheet">--}}
{{--    <link href="{{asset('css/bootstrap/google-fonts.css')}}" rel="stylesheet">--}}


    <!-- Included JS Files -->
    <script type="text/javascript" src="{{asset('js/newjs/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/newjs/custome.modernriz.js.js')}}"></script>
    <script type="text/javascript"> var notification = ''; </script>
</head>
<body>

{{--<div class="container">--}}
@include('frontend-layouts.menu')
{{--<section class="stem-body">--}}
@yield('content')
{{--</section>--}}
@include('frontend-layouts.footer')
{{--</div>--}}

<!-- js ---->

<!-- carouFredSel plugin -->
<script type="text/javascript" src="{{asset('js/newjs/jquery.carouFredSel-6.2.0-packed.js')}}"></script>
<script type="text/javascript" src="{{asset('js/newjs/jquery.touchSwip.min.js')}}"></script>
<!-- Scripts Initialize -->
<script type="text/javascript" src="{{asset('js/newjs/app-head-call.js')}}"></script>
<script type="text/javascript" src="{{asset('js/newjs/jquery.nivo.slider.pack.js')}}"></script>
<script type="text/javascript" src="{{asset('js/newjs/datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('js/newjs/foundation.min.js')}}"></script>
<script>$(document).foundation();</script>
<!-- Smallipop JS - Tooltips -->
<script type="text/javascript" src="{{asset('js/newjs/prettyify.js')}}"></script>
<script type="text/javascript" src="{{asset('js/newjs/jquery.smallipop.js')}}"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

<script type="text/javascript" src="{{asset('js/bootstrap/bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('js/frontend.js')}}"></script>

{{--<script type="text/javascript" src="{{asset('js/bootstrap/bootstrap.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('js/bootstrap/jquery.min.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('js/bootstrap/slim.min.js')}}"></script>--}}
<script>
    /*jshint jquery:true */
    jQuery.noConflict();

    jQuery(window).load(function () {
        "use strict";
        jQuery('#slider').nivoSlider({controlNav: false});
    });
    jQuery(document).ready(function () {
        "use strict";
        jQuery('input.datepicker').Zebra_DatePicker();
        // Carousel
        jQuery("#carousel-type1").carouFredSel({
            responsive: true,
            width: '100%',
            auto: false,
            circular: false,
            infinite: false,
            items: {
                visible: {min: 1, max: 4},
            },
            scroll: {
                items: 1,
                pauseOnHover: true
            },
            prev: {
                button: "#prev2",
                key: "left"
            },
            next: {
                button: "#next2",
                key: "right"
            },
            swipe: true
        });

        jQuery(".work_slide ul").carouFredSel({
            circular: false,
            infinite: true,
            auto: false,
            scroll: {items: 1},
            items: {visible: {min: 3, max: 3}},
            prev: {button: "#slide_prev", key: "left"},
            next: {button: "#slide_next", key: "right"}
        });
        jQuery("#testimonial_slide").carouFredSel({
            circular: false,
            infinite: true,
            auto: false,
            scroll: {items: 1},
            prev: {button: "#slide_prev1", key: "left"},
            next: {button: "#slide_next1", key: "right"}
        });


    });

</script>
<!-- Initialize JS Plugins -->
<script src="js/app-bottom-calls.js"></script>
<!-- End Main Wrapper -->

</body>
</html>
