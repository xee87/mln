


@extends('layouts.app', ['title' => __('User Management')])

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-md-12 col-sm-12 col-lg-4 col-xl-6">
                                <h3 class="mb-0">{{ __('Users') }}</h3>

                            </div>
                            <div class="col-md-12 col-sm-12 mt-2 mt-md-2 col-lg-4 text-right col-xl-3">
                                <form method="post" action="{{ route('user.search') }}" class=" navbar-search-light form-inline mr-sm-3" id="navbar-search-main">
                                    @csrf
                                    <div class="form-group mb-0">
                                        <div class="input-group input-group-alternative input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-search"></i></span>
                                            </div>
                                            <input class="form-control" value="{{ isset($keyword) ? $keyword : '' }}" name="search" placeholder="Search" type="text">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12 col-xs-12 text-md-left col-lg-4 mt-2 mt-md-2 text-xs-left text-lg-right col-xl-3">
                                <a href="{{ route('user.export') }}" class="btn btn-sm btn-primary">{{ __('Export Users') }}</a>
                                <a href="{{ route('user.create') }}" class="btn btn-sm btn-primary">{{ __('Add user') }}</a>
                            </div>
                        </div>
                    </div>

                    

                    @include('layouts.status.success')

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">{{ __('Name') }}</th>
                                    <th scope="col">{{ __('Email') }}</th>
                                    <th scope="col">{{ __('Pharmacy Name') }}</th>
                                    <th scope="col">{{ __('Pharmacy Phone') }}</th>
                                    <th scope="col">{{ __('Role') }}</th>
                                    <th scope="col">{{ __('Creation Date') }}</th>
                                    <th scope="col">{{ __('Status') }}</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td> <?php echo $user->name ? $user->name : '<i>NULL</i>'; ?> </td>
                                        <td>
                                            <a href="mailto:{{ $user->email }}">{{ $user->email }}</a>
                                        </td>
                                        <td> <?php echo $user->pharmacy_name ? $user->pharmacy_name : '<i>NULL</i>'; ?> </td>
                                        <td> <?php echo $user->pharmacy_phone ? $user->pharmacy_phone : '<i>NULL</i>'; ?> </td>
                                        <td> <?php echo $user->role ? $user->role : '<i>NULL</i>'; ?> </td>
                                        <td>{{ $user->created_at->format('d/m/Y H:i') }}</td>
                                        <td>{{ ($user->status == 1) ? "Active": "Inactive"  }}</td>
                                        <td class="text-right">
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                    @if ($user->id != auth()->id())
                                                        {{--<form action="{{ route('user.destroy', $user) }}" method="post">--}}
                                                            {{--@csrf--}}
                                                            {{--@method('delete')--}}
                                                            {{----}}
                                                            {{--<a class="dropdown-item" href="{{ route('user.edit', $user) }}">{{ __('Edit') }}</a>--}}
                                                            {{--<button type="button" class="dropdown-item" onclick="confirm('{{ __("Are you sure you want to delete this user?") }}') ? this.parentElement.submit() : ''">--}}
                                                                {{--{{ __('Delete') }}--}}
                                                            {{--</button>--}}
                                                        {{--</form>    --}}

                                                        @if($user->status == 1)
                                                            <a class="dropdown-item" href="{{ route('user.inactive', ['id' => $user->id]) }}">{{ __('Inctive') }}</a>
                                                        @elseif($user->status == 0)
                                                            <a class="dropdown-item" href="{{ route('user.active', ['id' => $user->id]) }}">{{ __('Active') }}</a>
                                                        @endif
                                                        <a class="dropdown-item" href="{{ route('user.edit', ['id' => $user->id]) }}">{{ __('Edit') }}</a>
                                                        <a class="dropdown-item" href="{{ route('user.delete', ['id' => $user->id]) }}">{{ __('Delete') }}</a>
                                                    @else
                                                        <a class="dropdown-item" href="{{ route('profile.edit') }}">{{ __('Edit') }}</a>
                                                    @endif
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            {{ $users->links() }}
                        </nav>
                    </div>
                </div>
            </div>
        </div>
            
        @include('layouts.footers.auth')
    </div>
@endsection