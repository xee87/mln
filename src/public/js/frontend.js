
if (typeof jQuery !== 'undefined')(function( window, document, $, undefined ){

    "use strict";

    var

        self = '',

        Frontend = {

            init: function(){
                self = this;
                self.statusPBSNumberChange();
                self.askAPIConfirmationSwal();

            },

            statusPBSNumberChange: function () {
                $('#yes-pbs').on('change', function(){

                    $('#pbs-number').css('display', 'block');

                });

                $('#no-pbs').on('change', function(){

                    $('#pbs-number').css('display', 'none');

                });
            },

            askAPIConfirmationSwal: function () {

                if($('#confirm-wholesaler').length > 0){

                    swal({
                            title: "Confirmation!",
                            text: "Is API your main wholesaler? ",
                            type: "success",
                            showCancelButton: true,
                            confirmButtonColor: '#25c9da',
                            confirmButtonText: 'Yes',
                            cancelButtonText: "No",
                            closeOnConfirm: true,
                            closeOnCancel: true
                        },
                        function(isConfirm){

                            if (!isConfirm){
                                window.location = $('#confirm-wholesaler').attr('data-cancelUrl');
                            }
                        });
                }
            }



        };

    $(document).ready(function(){
        Frontend.init();
    });

})( window, document, jQuery );



