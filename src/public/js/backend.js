

var exDateValue  = '';
var exFirstEmpty = lastRemoveDate = false;


$( document ).ready(function() {

    $('.update-inven').on('click', function(){

        var link = $(this).attr('data-href');
        $( document).find('.update-inventory-href').attr('href', link);

    });

    $('.timepicker').datetimepicker({
        format: 'H:mm',
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up text-warning",
            down: "fa fa-arrow-down text-warning"
        }

    });

    exceptionMultiDate();
    exeptionTimeCheckBox();
    passwordView();
    phoneFieldValidate();
   // verifyNumber();
    //checkCode();

    $('.agreementlabel').on('click', function (e) {
        e.preventDefault();
        var url = $(this).data('url');
        window.location.href = url;
    });

    $('.phoneField').inputmask();



    $('#whc-form').submit(function(e){

        e.preventDefault();
        var url = $(this).attr('action');
        var data = $(this).serialize();

        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            success: function(data){

                if($.isEmptyObject(data.error)){
                    location.reload();
                }else{
                    $('#whc-form').find('.invalid-feedback.code').removeClass('d-none').html('<strong>'+ data.error.code + '</strong>');
                    $('#whc-form').find('.form-group.code').addClass('has-danger');
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    });

    $('.update-wh').on('click', function(){

        var code = $(this).attr('data-code');
        var name = $(this).attr('data-name');
        var url = $(this).attr('data-href');

        var form = $('.whu-form');
        form.attr('action', url);
        form.find('.code-field').val(code);
        form.find('.name-field').val(name);
    });

    $('#yes-pbs').on('change', function(){

        $('#pbs-number').css('display', 'block');

    });

    $('#no-pbs').on('change', function(){

        $('#pbs-number').css('display', 'none');

    });

    if($('#confirm-wholesaler').length > 0){
            $('#confirm-wholesaler').modal('show');
    }

    if($('#termConditionModal').length > 0){
        $('#termConditionModal').modal('show');
    }

    $('#savePolicyModal').on('click', function(){

        var url = $(this).data('url');

        $.ajax({
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            success: function(data) {

                $('#termConditionModal').modal('toggle');

            }
        });

    });

});


function phoneFieldValidate() {
    $(document).on('submit', '#verifyPhoneForm', function (e) {
        var phone = $(this).find('.phoneField').val();
        if(phone.includes('_')){
            e.preventDefault();
            $('.phone-error').removeClass('d-none');
            $(this).find('.phoneField')
                .attr('style', 'border:1px solid red !important;').on('keyup', function () {
                $(this).attr('style', '');
                $('.phone-error').addClass('d-none');
            })
        }
    });
}

function verifyNumber() {
    var selector = $('#verifyPhoneForm');
    if(selector.is('*')){
        $(document).on('submit', '#verifyPhoneForm', function (e) {

            e.preventDefault();
            var phone = $('.phoneField').val().trim();
            var url = $(this).data('url');
            var id = $('.user_idF').val();

            $('#verifyNumber').modal('show');
            $.ajax({
                url: url,
                type: 'POST',
                data: {phone: phone, user_id: id},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data){

                    console.log(data);

                },
                error: function(error){
                    console.log(error);
                }
            });


        })
    }
}


function checkCode() {

    $(document).on('submit', '#verifyCode', function (e) {

        e.preventDefault();
        var form = $('#verifyCode');
        var url = form.attr('action');

        $('#verifyNumber').modal('show');
        $.ajax({
            url: url,
            type: 'POST',
            data: form.serialize(),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){

                console.log(data);

            },
            error: function(error){
                console.log(error);
            }
        });


    })
}


function passwordView() {
    $("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password .icon').addClass( "fa-eye-slash" );
            $('#show_hide_password .icon').removeClass( "fa-eye" );
        }else if($('#show_hide_password input').attr("type") == "password"){
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password .icon').removeClass( "fa-eye-slash" );
            $('#show_hide_password .icon').addClass( "fa-eye" );
        }
    });
}

function exceptionMultiDate(){

    var datepicker = $('#datepicker');
    datepicker.datepicker({
        altField: "#alternate",
        multidate: true,
        format: 'dd-mm-yyyy',
        startDate: new Date()

    }).on('changeDate', function (ev, d) {

        var allDatesSelector = $('.selected-dates');
        var value = datepicker.datepicker('getFormattedDate');
        var classUnique = getDateDP(exDateValue, value, false);
        var remove = getDateDP(exDateValue, value, true);
        if(exFirstEmpty){
            remove = false;
            exFirstEmpty = false;
        }

        exDateValue = value;

        if(remove){
            console.log('removed');
            allDatesSelector.find('.ts-'+  classUnique).remove();
            return;
        }

        var cloneHtml = $(".cloneDiv .timestamp").clone();

        cloneHtml.addClass('ts-'+ classUnique);
        cloneHtml.find('.exDateField').val(ev.format());
        cloneHtml.find('.form-control-label').text(ev.format());
        cloneHtml.find('#input-time').datetimepicker({
            format: 'H:mm',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up text-warning",
                down: "fa fa-arrow-down text-warning"
            }
        });
        allDatesSelector.append(cloneHtml);
    });

    var exceptionDatesJS = '';

    if($('.exceptionDatesJS').is('*')){
        exceptionDatesJS =  $('.exceptionDatesJS').val().trim();
    }


    if(exceptionDatesJS.length > 0){
        datepicker.datepicker('update');
        datepicker.data({date: exceptionDatesJS});
        datepicker.datepicker('update');
        datepicker.datepicker().children('input').val(exceptionDatesJS);
    }

    exDateValue = datepicker.datepicker('getFormattedDate');
    if(exDateValue.length == 0)
        exFirstEmpty = true;

    if (!$('#datetimeCheckbox').is(':checked')) {
        $('.calenderException').hide();
    }

}



function exeptionTimeCheckBox(){


    $('#datetimeCheckbox').on('change', function(){

        if($('#datetimeCheckbox').is(':checked')) {
            $('.calenderException').show();
            $('.timestamp .timepicker').each(function(){
                $(this).prop('required',true);
            });
        }else{
            $('.calenderException').hide();
            $('.timestamp .timepicker').each(function(){
                $(this).prop('required',false);
            });
        }

    })
}


function getDateDP(oldDates, newDates, checkAddOrRemove){

    var newDate = newDates;
    oldDates = oldDates.split(',');
    newDates = newDates.split(',');

    if(checkAddOrRemove){

        if( oldDates.length > newDates.length ){
            return true;
        }else if(oldDates.length === newDates.length && !exFirstEmpty){
            if(lastRemoveDate){
                lastRemoveDate = false;
                return false;
            }
            lastRemoveDate = true;
            return true;
        }

        return false;
    }

    let difference = oldDates
            .filter(x => !newDates.includes(x)).concat(newDates.filter(x => !oldDates.includes(x)));

    return difference[0].length > 0 ? difference[0] : newDate;
}


google.maps.event.addDomListener(window, 'load', initialize);

function initialize() {
    var input = document.getElementById('location');
    if(input){

        new google.maps.places.Autocomplete(input);
    }
}
