<?php

namespace App;

use Aws\Credentials\AssumeRoleCredentialProvider;
use Aws\Credentials\CredentialProvider;
use Aws\Credentials\InstanceProfileProvider;
use Aws\Sts\StsClient;
use Illuminate\Database\Eloquent\Model;
use Aws\CloudWatchEvents\CloudWatchEventsClient;
use Aws\Exception\AwsException;
use Aws\Kms;
use Illuminate\Support\Facades\Auth;
use App\Helper\PA;
use Aws\Kms\KmsClient;

class AWSCloudWatch extends Model
{

    public $client;

    public $australianTimeZone;

    public $earlyExecution = 5;

    public $kms;

    public function __construct(array $attributes)
    {
        parent::__construct($attributes);

        $this->client = new CloudWatchEventsClient([
            //'profile' => 'default',
            'region' => PA::getSettings('dataRegion', 'aws_settings'), //env('AWS_REGION', 'us-east-1'),
            'version' => '2015-10-07',
            'credentials' => [
                'key' => PA::getSettings('awsKey', 'aws_settings'), //env('AWS_KEY'),
                'secret' => PA::getSettings('awsSecret', 'aws_settings'), //env('AWS_SECRET_KEY')
            ]
            //'credentials' => $this->credsProvider()
        ]);

        $this->kms = new KmsClient([
            'credentials' => [
                'key' => PA::getSettings('awsKey', 'aws_settings'), //env('AWS_KEY'),
                'secret' => PA::getSettings('awsSecret', 'aws_settings'), //env('AWS_SECRET_KEY')
            ],
            'version' => '2014-11-01',
            'region'  => PA::getSettings('dataRegion', 'aws_settings')
        ]);

        $this->australianTimeZone = \DateTimeZone::listIdentifiers(\DateTimeZone::AUSTRALIA);

        if(!env('AWS_ACTIVE', true)) return;
    }


    public function cronExpression($params){

        $minutes = $params['minutes'];
        $hours = $params['hours'];
        $timeZone = $params['timeZone'];

        $dayOfWeek = !is_null($params['weekDays']) ? implode(',', $params['weekDays']) : '?';

        $dayOfMonth = is_null($params['weekDays']) ? '*' : '?';

        $year = $month = '*';

        $datetime = $hours .':'. $minutes;
        $tz_from = $timeZone;
        $tz_to = 'UTC';

        $dt = new \DateTime($datetime, new \DateTimeZone($tz_from));
        $dt->setTimeZone(new \DateTimeZone($tz_to));


        $dt->sub(new \DateInterval('PT' . $this->earlyExecution . 'M'));
        $hours = $dt->format("H");
        $minutes = $dt->format('i');

        //return 'cron(' . $minutes . ' ' . $hours . ' * *  *)';
        return 'cron(' . $minutes . ' ' . $hours . ' ' . $dayOfMonth . ' ' . $month . ' ' . $dayOfWeek . ' ' . $year . ')';
    }

    // Cloud Watch RULES.


    /**
     * It creates a new rule inside AWSCloudWatch Service.
     * @param null $ruleName
     * @param null $timeExpression
     * @throws \Exception
     */
    public function addRule($ruleName = null, $timeExpression = null){

        //Role ARN may be saved to DB
        //Schedule time is set by user.

        /*var_dump($this->cronExpression($timeExpression));
        exit;
        */

        try {
            $result = $this->client->putRule(array(
                'Name' => $ruleName, //'DEMO_RULE_NAME', // REQUIRED
                'RoleArn' => 'arn:aws:iam::552369715981:role/service-role/paCustomerCheckout-role-20fnrsy1', // IAM ROLE ARN
                //'ScheduleExpression' => 'rate(5 minutes)',
                'ScheduleExpression' => $this->cronExpression($timeExpression),
                'State' => 'ENABLED',
                //'' => ''
            ));
          //  var_dump($result);
        } catch (AwsException $e) {
            // output error message if fails
            throw new \Exception($e->getMessage());
        }
    }

    // It demonstrates how to turn on a rule for incoming events.
    // When you enable a rule, incoming events might not immediately start matching to a newly enabled rule. Allow a short period of time for changes to take effect.
    public function enableRule($ruleName = null){


        try {
            $client = $this->client;
            $result = $client->enableRule([
                'Name' =>  $ruleName, //'DEMO_RULE_NAME', // REQUIRED
            ]);
          //  var_dump($result);
        } catch (AwsException $e) {
            // output error message if fails
            error_log($e->getMessage());
        }

    }

    // It demonstrates how to details about a specific rule. @INFO: The description mentioned here is WRONG.
    public function disableRule($ruleName = null){

        try {
            $client = $this->client;
            $result = $client->disableRule([
                'Name' =>  $ruleName, //'DEMO_RULE_NAME', // REQUIRED
            ]);
           // var_dump($result);
        } catch (AwsException $e) {
            // output error message if fails
            error_log($e->getMessage());
        }

    }


    //Cloud Watch - EVENTS

    //It demonstrates how to send an event that is matched to targets for handling.
    public function addEvent(){

        try {
            $client = $this->client;
            $result = $client->putEvents([
                'Entries' => [ // REQUIRED
                    [
                        'Detail' => json_encode(['key'=>'4']),
                        'DetailType' => 'Test Event detail type',
                        'Resources' => ['events.amazonaws.com'],
                        'Source' => 'LaravelApp',
                        'Time' => time()
                    ],
                ],
            ]);
            //var_dump($result['Entries'][0]['EventId']); exit;
            return $result['Entries'][0]['EventId'];
        } catch (AwsException $e) {
            // output error message if fails
            error_log($e->getMessage());
        }
    }


    //Cloud Watch - TARGET
    //It demonstrates how to define a target to respond to an event.
    public function createTarget($ruleId, $targetId, $user, $pass){

        $object = new \stdClass();
        $object->u = $user;
        $object->p = $pass;
        $object->i = Auth::id();
        $args = [
            'u' => $user,
            'p' => $pass,
            'i' => Auth::id(),
        ];

        try {
            $client = $this->client;

            $result = $client->putTargets([
                'Rule' => $ruleId, // REQUIRED
                'Targets' => [ // REQUIRED
                    [
                        'Arn' => PA::getSettings('lambdaArn', 'aws_settings'), //'arn:aws:lambda:us-east-1:527501077686:function:jntTestLamda', // REQUIRED
                        'Id' => $targetId, //'f49f-3d3b-4777f4b822b9', // REQUIRED
                        //'Input' => json_encode($object)
                        'Input' => json_encode($args)
                        //'Id' => 'myCloudWatchEventsTarget' // REQUIRED
                    ],
                ],
            ]);
           // var_dump($result);
        } catch (AwsException $e) {
            // output error message if fails
            throw new \Exception($e->getMessage());
        }

    }

    public function encryptKMSPwd($pass){
        $kms = $this->kms;
        $enPass = $kms->encrypt([
            'KeyId' => PA::getSettings('kmsKeyId', 'aws_settings'),
            'Plaintext' => $pass,
        ]);
        return base64_encode($enPass["CiphertextBlob"]);
    }

    //Extra
    public function eventPatternTest(){

        $client = $this->client;



        $exampleEvent = '{
          "version": "0",
          "id": "6a7e8feb-b491-4cf7-a9f1-bf3703467718",
          "detail-type": "EC2 Instance State-change Notification",
          "source": "aws.ec2",
          "account": "111122223333",
          "time": "2015-12-22T18:43:48Z",
          "region": "us-east-2",
            "resources": [
                "arn:aws:ec2:us-east-2:123456789012:instance/i-12345678"
            ],
          "detail": {
              "instance-id": "i-12345678",
              "state": "terminated"
            }
          }';

        $exampleEventPattern = '{
          "source": [ "aws.ec2" ],
          "detail-type": [ "EC2 Instance State-change Notification" ],
          "detail": {
              "state": [ "terminated" ]
          }
        }';

        try {
            $result = $client->testEventPattern([
                'Event' => $exampleEvent,
                'EventPattern' => $exampleEventPattern
            ]);
            var_dump($result);
        } catch (AwsException $e) {
            // output error message if fails
            error_log($e->getMessage());
        }
    }
}