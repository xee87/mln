<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helper\PA;


class Purchase_items extends Model
{
    protected $updated_at = false;
    protected $created_at = false;
    public $timestamps = false;

    protected $perPage;

    protected $fillable = [
        "purchase_id", "sr_no", "pde", "name","list_price","discount","your_price","soh", "quantity", "subtotal_ex", "subtotal_inc"
    ];


    protected $table ="purchase_items";

    public function __construct($args = []){
        $this->perPage = PA::getPaginationPerPage();
        parent::__construct($args);
    }


}

