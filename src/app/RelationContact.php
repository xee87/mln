<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelationContact extends Model
{
    protected $updated_at = false;
    protected $created_at = false;


    protected $fillable=[
        "email"
    ];

    protected $table = "relation_contact";


}