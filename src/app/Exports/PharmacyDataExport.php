<?php

namespace App\Exports;


use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;


use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PharmacyDataExport implements FromView//FromQuery, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
     *
    */

  //  use Exportable;

    public function view(): View
    {
        $users = User::all();
        return view('exports.pharmacies', compact('users'));

    }

//    public function query()
//    {
//        return User::query()->select('name', 'email', 'pharmacy_name', 'pbs_number', 'pharmacy_phone', 'address', 'role');
//    }
//
//    public function headings(): array
//    {
//        return ["Pharmist Name", "Email", "Pharmacy Name", "Pharmacy Phone", "PBS Approval Number", "Pharmacy Address", "Role"];
//    }
}
