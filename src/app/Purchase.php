<?php

namespace App;

use App\Helper\PA;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;


class Purchase extends Model
{
    protected $updated_at = false;
    protected $created_at = false;
    /*public $timestamps = false;
    public $user_id = false;*/

    protected $perPage = 50;

    protected $guarded = [];

    protected $fillable = [
        "user_id", "timestamp", "message", "status",
    ];


    protected $table ="purchase";

    public function __construct($args = []){

        $this->perPage = PA::getPaginationPerPage();

        parent::__construct($args);
    }

    protected static function boot() {

        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('id', 'desc');
        });

        parent::boot();

    }

}
