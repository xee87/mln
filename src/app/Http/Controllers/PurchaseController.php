<?php

namespace App\Http\Controllers;

use App\Helper\PA;
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\PasswordRequest;
use App\User;
use Illuminate\Http\Request;
use App\Purchase;
use App\Purchase_items;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;


class PurchaseController extends Controller
{
    /**
     * Show the form for editing the profile.
     *
     * @return \Illuminate\View\View
     */
    public function index(Purchase $model)
    {
        //$purchases = Purchase::where('user_id', Auth::id())->get();
        return view('purchase.index', ['purchases' => $model->where('user_id', Auth::id())->paginate()]);
    }

    public function purchase_items(Purchase_items $model, $id){
        $items = $model->where('purchase_id', $id)->paginate();
        return view('purchase.items', compact('items'));
    }

    public function save_purchase(Request $request){

        try {

            Log::info('Entered in Save Purchase Method');

            $data = [
                'user_id' => $request->id,
                'timestamp' => $request->timestamp,
                'message' => (is_array($request->msg) && !empty($request->msg)) ? explode(',', $request->msg[0]) : '',
                'status' => $request->status,
                'items' => []
            ];

            $user = User::where('id', $request->id)->first();

            if(is_object($user)) {

                $testData = isset($request->testData) && $request->testData == true ? true : false;

                $userName = $user->name;
                $userEmail = (!$testData) ? $user->email : 'zeshans87@live.com';
                $data['name'] = $user->name;

                if (!$testData) $purchase = Purchase::create($data);

                $items = array();

                if (!empty($request->data) && count($request->data) > 0) {

                    foreach ($request->data as $key => $item) {
                        array_push($items, array(
                            'purchase_id' => (!$testData) ? $purchase->id : 0,
                            'sr_no' => $item[0],
                            'pde' => $item[1],
                            'name' => $item[2],
                            'list_price' => $item[3],
                            'discount' => $item[4],
                            'your_price' => $item[5],
                            'soh' => $item[6],
                            'quantity' => $item[7],
                            'subtotal_ex' => $item[8],
                            'subtotal_inc' => $item[9],
                        ));
                    }

                    if (!$testData) Purchase_items::insert($items);

                    usort($items, function ($items, $b) {
                        return strcmp($items["name"], $b["name"]);
                    });


                    $data['items'] = $items;



                }

                if (env('MAIL_SEND') && (int)trim($data['status']) == 1) {

                    Mail::send('email_templates.purchase', $data, function ($message) use ($userEmail, $userName) {
                        $message->to($userEmail, $userName)->subject
                        (env('APP_NAME') . ' Purchased Items');
                    });

                    if(env('DEV_TEST_EMAIL', false)) {

                        $userEmail = 'johnnuman@live.com';
                        $userName = 'John';

                        Mail::send('email_templates.purchase', $data, function ($message) use ($userEmail, $userName) {
                            $message->to($userEmail, $userName)->subject
                            (env('APP_NAME') . ' Purchased Items');
                        });

                        $userEmail = 'zeshans87@live.com';
                        $userName = 'Zeeshan';

                        Mail::send('email_templates.purchase', $data, function ($message) use ($userEmail, $userName) {
                            $message->to($userEmail, $userName)->subject
                            (env('APP_NAME') . ' Purchased Items');
                        });
                    }
                }

                if (env('MAIL_SEND') && (int)trim($data['status']) == 0) {

                    Mail::send(
                        'email_templates.failedexecution',
                        $data,
                        function ($message) use ($userEmail, $userName) {
                            $message
                                ->to($userEmail, $userName)
                                ->subject('Failed Execution');
                        }
                    );

                    if(env('SEND_SMS', false)){
                        PA::sendSMS($user->id, 'We are NOT pleased to inform you, the automatic cart submission failed. Please submit cart manually. Team ' . env('APP_NAME') );
                    }

                    if(env('DEV_TEST_EMAIL', false)) {

                        $userEmail = 'johnnuman@live.com';
                        $userName = 'John';

                        Mail::send(
                            'email_templates.failedexecution',
                            $data,
                            function ($message) use ($userEmail, $userName) {
                                $message
                                    ->to($userEmail, $userName)
                                    ->subject('Failed Execution');
                            }
                        );

                        $userEmail = 'zeshans87@live.com';
                        $userName = 'Zeeshan';

                        Mail::send(
                            'email_templates.failedexecution',
                            $data,
                            function ($message) use ($userEmail, $userName) {
                                $message
                                    ->to($userEmail, $userName)
                                    ->subject('Failed Execution');
                            }
                        );
                    }
                }

            }

            return response()->json(['message' => 'Data saved successfully!'])->setStatusCode(200);

        }catch (\Exception $e){
            $logString  = 'Message: ' . $e->getMessage().'\n';
            $logString .= 'Line: ' . $e->getLine().'\n';
            $logString .= 'File: ' . $e->getFile().'\n';

            Log::info($logString);

            return response()->json(['message' => 'Data saved successfully-not.'])->setStatusCode(200);
        }

    }

    public function debug(){


    }



}
