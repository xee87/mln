<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileRequest;
use App\Http\Requests\PasswordRequest;
use Illuminate\Http\Request;
use App\Options;
use Illuminate\Support\Facades\Hash;

class SettingController extends Controller
{
    /**
     * Show the form for editing the profile.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $aws_setting = Options::where('option_name', 'aws_settings')->first();
        $gen_setting = Options::where('option_name', 'general_settings')->first();
        if($aws_setting)
            $aws_setting = unserialize($aws_setting->option_value);
        if($gen_setting)
            $gen_setting = unserialize($gen_setting->option_value);
        return view('settings.index', compact('aws_setting', 'gen_setting'));

    }

    public function update_options(Request $request){


        $value = serialize($request->except(['option_name', '_token']));
        $name = $request->option_name;



        Options::updateOrCreate(['option_name' => $name],['option_name' => $name, 'option_value' => $value]);
        return redirect()->back()->withStatus(ucfirst(str_replace('_', ' ', $name)) . __( ' successfully saved!'));

    }

    public function emailSettings(){
        $et = Options::where('option_name', 'email_templates')->first();
        if($et)
            $et = unserialize($et->option_value);

        return view('email_templates.index', compact('et'));
    }

    /**
     * Update the profile
     *
     * @param  \App\Http\Requests\ProfileRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */

}
