<?php

namespace App\Http\Controllers;

use App\User;
use App\AWSCloudWatch;
use App\Http\Requests\UserRequest;
use App\User_settings;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;

use App\Exports\PharmacyDataExport;
use Maatwebsite\Excel\Facades\Excel;
use Mail;

class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\User  $model
     * @return \Illuminate\View\View
     */
    public function index(User $model)
    {
        return view('users.index', ['users' => $model->where('status', '!=', 2)->paginate(15)]);
    }

    public function nonwholesalers(User $model)
    {
        return view('users.non-wholesalers', ['users' => $model->where('user_state', 1)->paginate(15)]);
    }

    public function export()
    {
        return Excel::download(new PharmacyDataExport, 'PharmacyRegistrationData.csv', \Maatwebsite\Excel\Excel::CSV, [
            'Content-Type' => 'text/csv',
        ]);
    }
    /**
     * Show the form for creating a new user
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created user in storage
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\User  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserRequest $request, User $model)
    {
        $user = $model->create($request->merge(['password' => Hash::make($request->get('password'))])->all());
        $user->assignRole('User');

        return redirect()->route('user.index')->withStatus(__('User successfully created.'));
    }

    /**
     * Show the form for editing the specified user
     *
     * @param  \App\User  $user
     * @return \Illuminate\View\View
     */
    public function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }

    public function editUser($id){

        $user = User::where('id', $id)->first();
        return view('users.edit', compact('user'));

    }

    /**
     * Update the specified user in storage
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserRequest $request, User  $user)
    {
        $user->update(
            $request->merge(['password' => Hash::make($request->get('password'))])
                ->except([$request->get('password') ? '' : 'password']
        ));

        return redirect()->route('user.index')->withStatus(__('User successfully updated.'));
    }

    public function active($id){

        $settings = User_settings::where('user_id', $id)->first();
        $ruleName = $settings->ruleId;
        $cloudWatch = new AWSCloudWatch([]);
        $cloudWatch->enableRule($ruleName);

        //@TODO: Need to Target update as per user settings.

        User::where('id', $id)->update(['status' => 1]);
        return redirect()->route('user.index')->withStatus(__('User successfully Activated.'));
    }

    public function inactive($id){

        $settings = User_settings::where('user_id', $id)->first();
        $ruleName = $settings->ruleId;
        $cloudWatch = new AWSCloudWatch([]);
        $cloudWatch->disableRule($ruleName);

        //@TODO: Need to Target update as per user settings.

        User::where('id', $id)->update(['status' => 0]);
        return redirect()->route('user.index')->withStatus(__('User successfully Inactive.'));
    }

    public function search(Request $request){
        $keyword = $request->search;
        $users = User::where('status', '!=', 2)->where('pharmacy_phone', 'like', '%' . $keyword . '%')->paginate(15);
        return view('users.index', compact('users', 'keyword'));
    }


    public function delete($id){

        $settings = User_settings::where('user_id', $id)->first();
        $ruleName = $settings->ruleId;
        $cloudWatch = new AWSCloudWatch([]);
        $cloudWatch->disableRule($ruleName);

        //@TODO: Need to Target update as per user settings.

        User::where('id', $id)->update(['status' => 2]);
        return redirect()->route('user.index')->withStatus(__('User successfully Deleted.'));
    }

    /**
     * Remove the specified user from storage
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(User  $user)
    {
        $user->delete();

        return redirect()->route('user.index')->withStatus(__('User successfully deleted.'));
    }

    public function info($id){

        $user = User::find($id);


        if($user->user_state == 1)
            return redirect()->route('register.message', ['id' => $user->id]);

        return view('frontend.register-info', compact('user'));

    }

    public function store_info(Request $request){

        $info = $request->except('_token', 'user_id', 'pbs');
        $info['pharmacy_phone'] = preg_replace('/\s+/', '', $info['pharmacy_phone']);
        User::where('id', $request->user_id)->update($info);
        $user = User::find($request->user_id);

        $userName = $user->name;
        $userEmail = $user->email;
        $data['name'] = $user->name;
        $data['days'] = 2;

        if (env('MAIL_SEND')) {
            Mail::send('email_templates.accountconfirmation', $data, function ($message) use ($userEmail, $userName) {
                $message->to($userEmail, $userName)->subject
                ('Account Activated');
            });
        }

       // auth()->login($user, true);
        return redirect()->route('user.login')->withStatus(__('Your account is activated, please login to continue.'));
    }

    public function messageView($id){
        User::where('id', $id)->update(['user_state' => 1, 'status' => 0]);
        $message = 'Other wholesalers are not supported at this stage, we will inform you once they are supported';
        return view('frontend.messages', compact('message'));
    }

    public function agreementPdf(){
        $user = User::find(auth()->user()->id);
        $pdf = \PDF::loadView('pdf.saas_agreement', compact('user'));
        return $pdf->download('saas_services_agreement.pdf');
    }

}
