<?php

namespace App\Http\Controllers;


use App\Mail\SendEmailMailable;
use App\RelationContact;
use App\User;
use \Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware(['auth']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('frontend.home');
    }

    public function dashboard(){

        if(auth()->id()){
            return view('blank');
        }else{
            return view('frontend.home');
        }
    }

    public function about_us(){
        return view('frontend.about-us');
    }

    public function contact_us(){
        return view('frontend.contact-us');
    }

    public function contactSave(Request $request){

        $data = $request->except('_token');

       // var_dump($data); exit;

        try{
            $name = $request->name;
            $adminMail = env('MAIL_FROM_ADDRESS');
            $userMail = $request->email;

            if(env('MAIL_SEND')){

                Mail::send('email_templates.contact_user', $data, function ($message) use ($userMail, $name) {
                    $message->to($userMail, $name)->subject
                    ('Thank you for contacting us!');
                });

                Mail::send('email_templates.contact_us', $data, function ($message) use ($adminMail, $name) {
                    $message->to($adminMail, $name)->subject
                    ('Note to Self');
                });
            }
        }catch (\Exception $e){
            return response()->json(['message' => $e->getMessage(), 'type' => 'error']);
        }
       // return response()->json(['message' => 'Success, We have received your mail.', 'type' => 'success', 'contact_form' => true]);
        return redirect()->back()->withStatus('Success, We have received your mail.');
        }

    public function faq(){
        return view('frontend.faq');
    }


    public function register(){
        return view('frontend.register');
    }


    public function login(){
        return view('frontend.login');
    }


    public function ajaxTermsCondition(Request $request){
        if(User::where('id', auth()->id())->update(['user_state' => 2])){
            return response()->json(['success'=>1]);
        }
    }

    public function emailTesting(){
        return view('email-test');
    }

    public function emailGet(Request $request){

        try{
            $data['name'] = 'Zeeshan';
            $data['days'] = 7;
            $data['link'] = '';
            $email = new SendEmailMailable();
            $email->setMail('email_templates.expiretriallreminder', $data, 'Reminder Trial Expire');

            Mail::to($request->email)->queue($email);
        }catch (\Exception $exception){
            return redirect()->back()->with(['error' => $exception->getMessage()]);
        }
        return redirect()->back()->with(['success' => 'email sent']);

    }

    public function emailUser(Request $request){

        $data = array(
            'email' => $request->email,
        );

        RelationContact::create($data);

        return redirect()->back()->with(['success' =>'email received']);

    }

    public  function ajaxRequestP0st(Request $request){
        $input= $request->all();
        return response() -> json(['success'=>'Got Simple Ajax Request']);
    }

    public function debug(){

    }
}
