<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileRequest;
use App\Http\Requests\PasswordRequest;
use Illuminate\Http\Request;
use App\User_settings;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\AWSCloudWatch;
use Illuminate\Support\Facades\Validator;

//use Twilio\Jwt\ClientToken;
//use GuzzleHttp\Exception\GuzzleException;
//use GuzzleHttp\Client;

class UserSettingController extends Controller
{
    /**
     * Show the form for editing the profile.
     *
     * @return \Illuminate\View\View
     */

    public function __construct()
    {
     //   ini_set('xdebug.var_display_max_depth', '-1');
    }

    public function index(){

        $cloudWatch = new AWSCloudWatch([]);
        $settings = User_settings::where('user_id', Auth::id())->first();
        $timezone = $cloudWatch->australianTimeZone;
        $oldTimeSelected = $oldDateSelected = $weekDays = [];

        if($settings){
            if(!is_null($settings->exception_date)){
                $dateTimeArray = unserialize($settings->exception_date);
                foreach($dateTimeArray as $k => $dt){
                    $dt = explode(' ', $dt);
                    $oldTimeSelected[] = $dt[1];
                    $oldDateSelected[] = $dt[0];
                }
            }

            if(!is_null($settings->week_days)){
                $weekDays = unserialize($settings->week_days);
            }
        }

        return view('user_settings.index', compact('settings','timezone', 'oldDateSelected', 'oldTimeSelected', 'weekDays'));

    }

    public function create(Request $request){

        $settings = $request->all();

        Validator::make($settings, [
            // 'name' => [ 'string', 'max:255'],
            'username' => ['required', 'string','max:255', 'unique:user_settings'],
        ])->validate();

        $ruleName = 'jnt-' . Auth::id(). '-' . uniqid();



        $weekDays = $request->days;

        $isExceptionDate = $request->exceptionTime;
        $exceptionDateTime = null;

        if(!is_null($isExceptionDate) && isset($request->extime)){
            $exceptionDateTime = [];
            foreach($request->exdate as $key => $date){
                $exceptionDateTime[] = $date . ' ' . $request->extime[$key];
            }
        }

        $cloudWatch = new AWSCloudWatch([]);
        $cloudWatch->addRule($ruleName, $this->_setExecutionTime($request));
        ($request->status == 1) ? $cloudWatch->enableRule($ruleName) : $cloudWatch->disableRule($ruleName);

         $targetId = 'jnt-' .uniqid() . '-' . uniqid() . uniqid();
         $pass = $cloudWatch->encryptKMSPwd($request->password);
         $cloudWatch->createTarget($ruleName, $targetId, $request->username, $pass);

        $settings = $request->all();
        $settings['password'] = $pass;

        $settings['exception_date'] =  is_null($exceptionDateTime) ? null: serialize($exceptionDateTime);
        $settings['week_days'] = is_null($weekDays) ? null: serialize($weekDays);

         User_settings::create(
             array_merge(
                 $settings,
                 [
                     'user_id' => Auth::id(),
                     'targetId' => $targetId,
                     'ruleId' => $ruleName
                 ]
             )
         );

         return back()->withStatus('Settings Created Successfully!');

    }

    public function update(Request $request){

        $weekDays = $request->days;
        $isExceptionDate = $request->exceptionTime;
        $exceptionDateTime = null;
        if(!is_null($isExceptionDate) && isset($request->extime)){
            $exceptionDateTime = [];
            foreach($request->exdate as $key => $date){
                $exceptionDateTime[] = $date . ' ' . $request->extime[$key];
            }
        }

        $settings = User_settings::where('user_id', Auth::id())->first();
        $oldTime = explode(':', $settings->time);
        $oldTime = $oldTime[0] . ':' . $oldTime[1];

        $weekDaysChange = $this->_isWeekDaysChanged($settings->week_days, $request);

        $ruleName = $settings->ruleId;
        $targetId = $settings->targetId;

        $cloudWatch = new AWSCloudWatch([]);

        if($settings->status != $request->status){
            ($request->status == 1) ? $cloudWatch->enableRule($ruleName) : $cloudWatch->disableRule($ruleName);
        }

        $toBeSaved = $request->except('_token', 'days', 'exceptionDatesJS', 'exceptionTime', 'exdate', 'extime');

        if(trim($settings->password) <> trim($request->password)){
            $request->password = $cloudWatch->encryptKMSPwd($request->password);
            $toBeSaved['password'] = $request->password;
        }

        if(
            (
                $oldTime <> $request->time ||
                trim($settings->url) <> trim($request->url) ||
                trim($settings->username) <> trim($request->username) ||
                trim($settings->password) <> trim($request->password) ||
                $weekDaysChange ||
                trim($settings->timezone) <> trim($request->timezone)
            )
            && $request->status == 1
        ){

            $cloudWatch->addRule($ruleName, $this->_setExecutionTime($request));
            $cloudWatch->enableRule($ruleName);
            $cloudWatch->createTarget($ruleName, $targetId, $request->username, $request->password);

        }

        $toBeSaved['exception_date'] =  is_null($exceptionDateTime) ? null : serialize($exceptionDateTime);
        $toBeSaved['week_days'] = is_null($weekDays) ? null : serialize($weekDays);

        User_settings::where('user_id', Auth::id())->update(array_merge($toBeSaved, ['user_id' => Auth::id()]));
        return back()->withStatus('Settings Updated Successfully!');

    }

    private function _setExecutionTime($args){

        $executionTime = $args->time;
        $executionTime = explode(':', $executionTime);

        $executionTime['minutes'] = isset($executionTime[1]) ? $executionTime[1] : '00';
        $executionTime['hours'] = $executionTime[0];
        $executionTime['timeZone'] = $args->timezone;
        $executionTime['weekDays'] = isset($args->days) ? $args->days : null;

        return $executionTime;
    }

    private function _isWeekDaysChanged($dbValue,$requestData){

        $oldData = (strlen($dbValue) > 0) ? \Opis\Closure\unserialize($dbValue) : [];
        $postData = isset($requestData->days) && count($requestData->days) > 0 ? $requestData->days : [];

        return ($oldData === $postData) ? false : true;
    }



}
