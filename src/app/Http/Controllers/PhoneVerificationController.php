<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Twilio\TwiML\VoiceResponse;

class PhoneVerificationController extends Controller
{
    public function show(Request $request)
    {
        return $request->user()->hasVerifiedPhone()
            ? redirect()->route('home')
            : view('verifyphone');
    }

    public function callToVerify($user_id, $phone)
    {
        $code = random_int(100000, 999999);

        $user = User::find($user_id);
        $user->forceFill([
            'verification_code' => $code
        ])->save();

        $client = new Client(env('TWILIO_SID'), env('TWILIO_AUTH_TOKEN'));
        $client->messages->create(
            '+61417401690',
            [
                'from' => '+61417401690',
                'body' => $code,
            ]
        );
    }

    public function ajaxSendVerifyCode(Request $request){

        $this->callToVerify($request->user_id ,$request->phone);

        return response()->json(['success' => true]);

    }

    public function verify(Request $request)
    {
        $user = User::find($request->user_id);
        if ($user->verification_code !== $request->code) {
//            throw ValidationException::withMessages([
//                'code' => ['The code your provided is wrong. Please try again or request another call.'],
//            ]);
            return response()->json(['success' => false, 'msg' => 'The code your provided is wrong. Please try again or request another code.']);
        }

        if ($user->hasVerifiedPhone()) {
            return response()->json(['success' => true]);
        }

        $user->markPhoneAsVerified();
        return response()->json(['success' => true]);
    }

    public function buildTwiML($code)
    {
        $code = $this->formatCode($code);
        $response = new VoiceResponse();
        $response->say("Hi, thanks for Joining. This is your verification code. {$code}. I repeat, {$code}.");
        echo $response;
    }

    public function formatCode($code)
    {
        $collection = collect(str_split($code));
        return $collection->reduce(
            function ($carry, $item) {
                return "{$carry}. {$item}";
            }
        );
    }
}
