<?php

namespace App\Http\Controllers;

use App\Helper\PA;
use Illuminate\Http\Request;
use App\Jobs\ExpireTrailJob;
use Carbon\Carbon;
use Mail;
use App\Mail\SendEmailMailable;

class JobController extends Controller
{
    //

    public function processQueue()
    {

        $emailJob = new ExpireTrailJob();
       // $emailJob->delay(Carbon::now()->addSeconds(5));
        dispatch($emailJob);
    }
}
