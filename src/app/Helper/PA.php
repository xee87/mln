<?php
/**
 * Created by PhpStorm.
 * User: zesha
 * Date: 12/8/2019
 * Time: 3:55 PM
 */
namespace App\Helper;

use App\Options;
use App\Purchase_items;
use App\User;
use App\User_settings;
use Carbon\Carbon;
use Dompdf\Exception;
use Twilio\Rest\Client;
use Faker\Provider\zh_CN\DateTime;

class PA{

    public static function formatFormLabel($label = null){

        if(is_null($label)) return '';

        $labelParts = preg_split('/(?=[A-Z])/',$label);

        return ucfirst(implode(' ', $labelParts));

    }

    public static function getPaginationPerPage(){
        return 50;
    }

    public static function getSettings($field = null, $option_name){

        $settings = Options::where('option_name', $option_name)->first();

        if(is_object($settings)) {
            $settings = unserialize($settings->option_value);

            if ($field == null) {
                return $settings;
            }

            return (isset($settings[$field])) ? $settings[$field] : null;
        }else{
            return null;
        }
    }

    public static function sendSMS($user_id, $message){
        try {
            $user = User::find($user_id);
            if (!is_null($user->pharmacy_phone)) {
                $client = new Client(env('TWILIO_SID'), env('TWILIO_AUTH_TOKEN'));
                $client->messages->create(
                    $user->pharmacy_phone,
                    [
                        'from' => env('TWILLIO_NUMBER'),
                        'body' => $message,
                    ]
                );
            }
        }catch (Exception $e){

        }
    }

    public static function getStatus($status){

        if($status == 0){
            return 'fail';
        }elseif($status == 1){
            return 'success';
        }elseif($status == 2){
            return 'partial success';
        }

        return null;

    }

    public static function totalPurchaseItems($id){
        return Purchase_items::where('purchase_id', $id)->count();
    }

    public static function dateFormat($date, $format = null){

        if(!empty($date)){
            $date=date_create($date);
            return $date = date_format($date,!is_null($format) ? $format : "M d, Y H:i:s");
        }else{
            return 'not set';
        }

    }

    public static function cleanTimeZoneName($string){

        $impuritiesList = ['/', '_'];

        foreach($impuritiesList as $impurity){

            $string = str_replace($impurity, ' ' ,$string);
        }

        return $string;
    }

    public static function getUserSettings($userId, $field=null){

        $user = User_settings::where('user_id',$userId)->first();

        if($user){
            if(!is_null($field)){
                return $user->$field;
            }

            return $user;
        }

        return false;

    }

    public static function getTimeZoneBasedDate($timeStamp, $userid = null){

        if(!$timeStamp) return '-';

        $datetime = gmdate("Y-m-d\TH:i:s\Z",$timeStamp);
        $userSettings = User_settings::where('user_id', is_null($userid) ? auth()->user()->id : $userid)->first();

        $tz_from = 'UTC';

        $tz_to = $userSettings->timezone;

        $dt = new \DateTime($datetime, new \DateTimeZone($tz_from));
        $dt->setTimeZone(new \DateTimeZone($tz_to));

        return $dt->format('d/m/Y h:i a');
    }

    public static function agreeOnTerms(){
        if(User::where(['id' => auth()->id(), 'user_state' => 2])->count()){
            return true;
        }
        return false;
    }

    public static function dateDifferenceDays($start, $end){
        $start = Carbon::parse($start);
        $end = Carbon::parse($end);
        return $start->diffInDays($end);
    }

    public static function trialNotify(){

        if(auth()->id()){
            $trailDays = self::getSettings('trialDays', 'general_settings');
            $user = User::where('id', auth()->id())->where('trail_expires', '!=', 2)->first();
            if($user){
                $days = self::dateDifferenceDays($user->email_verified_at, now());
                if((int)$days <= (int)$trailDays){
                    return $days;
                }
            }
            return false;
        }

        return false;
    }

}
