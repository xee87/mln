<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailMailable extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $view;
    public $subject;
    public $dataView;

    public function __construct()
    {
        //
    }
    /**
     * Build the message.
     *
     * @return $this
     */

    public function setMail($view, $data='', $subject = ''){
        $this->view = $view;
        $this->subject = $subject;
        $this->dataView = $data;
    }

    public function build()
    {
        return $this->subject($this->subject)->view($this->view, $this->dataView);
    }
}
