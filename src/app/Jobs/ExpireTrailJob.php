<?php

namespace App\Jobs;

use App\Helper\PA;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Support\Facades\Log;
use Mail;
use App\Mail\SendEmailMailable;
use PHPUnit\Framework\Exception;

class ExpireTrailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try{
            $users = User::where(['status' => 1, 'user_state' => 2])->where('trail_expires', '!=', 2);

            if($users->count() > 0){

                $users = $users->get();
                foreach($users as $k => $user){

                    if(!is_null($user->email_verified_at)){

                        $days = PA::dateDifferenceDays($user->email_verified_at, now());
                        $trailDays = PA::getSettings('trialDays', 'general_settings');
                        $data['name'] = $user->name;
                        $data['days'] = $trailDays;

                        if($days == 2 && $user->trail_expires == 0){
                            User::where('id', $user->id)->update(['trail_expires' => 1]);
                            $email = new SendEmailMailable();
                            $email->setMail('email_templates.expiretriallreminder', $data, 'Reminder Trial Expire') ;
                            Mail::to($user->email)->queue($email);
                            continue;
                        }

                        if($days >= $trailDays){
                            User::where('id', $user->id)->update(['status' => 0, 'trail_expires' => 2]);
                            $email = new SendEmailMailable();
                            $email->setMail('email_templates.expiretrial', $data, 'Trial Expire') ;
                            Mail::to($user->email)->queue($email);
                        }

                    }
                }
            }
        }catch (Exception $e){
            Log::error($e->getMessage());
        }



    }
}
