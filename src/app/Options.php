<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Options extends Model
{
    protected $updated_at = false;
    protected $created_at = false;
    public $timestamps = false;

    protected $fillable = [
        "option_name", "option_value",
    ];

    protected $table ="options";


}
