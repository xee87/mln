<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_settings extends Model
{
    protected $updated_at = false;
    protected $created_at = false;
    public $timestamps = false;

    //protected $dates = ['time'];

    protected $fillable = [
        "user_id", "username", "password", "url", "status", "targetId", "ruleId", "time", "timezone", "exception_date", "week_days"
    ];

    protected $table ="user_settings";

}
